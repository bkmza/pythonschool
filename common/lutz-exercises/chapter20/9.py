import math

def sqrt_seq_for(seq):
  new = []
  for item in seq:
    new.append(math.sqrt(item))
  return new

def sqrt_seq_math(seq):
  return map(math.sqrt, seq)

def sqrt_seq_gen(seq):
  new = [math.sqrt(x) for x in seq]
  return new

print sqrt_seq_for([2, 4, 9, 6, 25])
print sqrt_seq_math([2,4,9,6,25])
print sqrt_seq_gen([2,4,9,6,25])