import csv

from Purchase import Purchase
from PricePurchase import PricePurchase
from BulkPurchase import BulkPurchase

def get_purchases(filename):
    with open(filename, 'r') as input_file:
        return [get_class_from_factory(rec) for rec in csv.reader(input_file, delimiter=';')]

def get_class_from_factory(rec):
    ctype = rec[0]
    name = rec[1]
    price = float(rec[2])
    qty = int(rec[3])    
    if   ctype == "Purchase": purchase = Purchase (name, price, qty)
    else:
        discount = float(rec[4])
        if   ctype == "PricePurchase": purchase = PricePurchase (name, price, qty, discount)
        elif ctype == "BulkPurchase":  purchase = BulkPurchase  (name, price, qty, discount)             
    return purchase

def proccess(filename):    
    purchases = get_purchases(filename)    
    if purchases:
        equal = True
        max_purchase = purchases[0]   
        for purchase in purchases:          
            print purchase            
            if not purchase.__dict__ == max_purchase.__dict__:
                if purchase.get_cost() > max_purchase.get_cost():
                    max_purchase = purchase
                equal = False   
        print "max purchase = {0}".format(max_purchase)
        if equal:
            print "all purchases equal"        
        print "--------------------------------"

def main():
    proccess("in1.csv")
    proccess("in2.csv")
    proccess("in3.csv")    
                
if __name__ == '__main__':
    main()