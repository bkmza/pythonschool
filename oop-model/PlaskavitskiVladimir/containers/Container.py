from abc import abstractmethod
from decorators import repr

@repr
class Container():

    @property
    def content(self):
        return self.__content

    def set_content(self, content):
        if self.check(content):
            self.__content = content
            return True
        return False
        #else:
        #    raise Exception("Opps! Container broke!")

    @property
    def weight(self):
        return self.__weight + (self.content.weight if self.content else 0)

    @property
    def out_metrics(self):
        return self.__out_metrics

    @property
    def in_metrics(self):
        return self.__in_metrics

    @abstractmethod
    def check(self, content):
        pass

    def __init__(self, weight, content, in_metrics, out_metrics):
        self.__weight = weight
        self.__content = content
        self.__in_metrics = in_metrics
        self.__out_metrics = out_metrics