import sys
import re
import math

REGEX_FLOAT = r'([eE\d.-]+)'
REGEX_POINT = r'\(\s*%s\s*\;\s*%s\s*\)' % (REGEX_FLOAT, REGEX_FLOAT)
REGEX_LINE = r'\s*%s\s*%s\s*' % (REGEX_POINT, REGEX_POINT)

def process(file_in, sequence):
  try:
      input_file = open(file_in, "r")
  except:
      return 'Input file not found'
    
  for line in input_file:
    for x1, y1, x2, y2 in re.findall(REGEX_LINE, line):
      len = get_line_length(x1,y1,x2,y2)
      rlen = int(round(len))
      append_to_sequence(sequence, rlen)
  
  input_file.close()
  return sorted(sequence)

def append_to_sequence(sequence, len):
  if isinstance(sequence, set):
    sequence.add(len)
  elif isinstance(sequence, list):
    if len not in sequence:
      sequence.append(len)

def get_line_length(x1,y1,x2,y2):
  return math.sqrt((float(x1) - float(x2)) ** 2 + ((float(y1) - float(y2)) ** 2))

      
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  if len(sys.argv) != 2:
    print 'usage: ./sequences12.py {--list | --set}'
    sys.exit(1)

  structure = [] if sys.argv[1] == '--list' else set()
  
  print 'sequences12 by %s' % structure
  test(process('not_exists_file.txt', structure), 'Input file not found')
  test(process('in.txt', structure), [1, 3])

if __name__ == '__main__':
  main()

