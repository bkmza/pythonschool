from PurchaseDiscount import PurchaseDiscount

class BulkPurchase(PurchaseDiscount):
              
    LIMIT = 2
    
    def __init__(self, name, price, qty, discount):
        PurchaseDiscount.__init__(self, name, price, qty, discount)
    
    def is_discount(self):        
        return self.qty >= self.LIMIT
    
    def calculating_discount(self):        
        return self.price * self.qty * self.discount/100 if self.is_discount() else 0