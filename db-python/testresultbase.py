from datetime import datetime
from abc import ABCMeta, abstractmethod
from testresultexception import TestResultException
from utils import test

DAY_FORMAT = '%d'
MONTH_FORMAT = '%m'
YEAR_LONG_FORMAT = '%Y'
DATE_SEP = '/'
DATE_FORMAT = '%s%s%s%s%s' % (DAY_FORMAT, DATE_SEP, MONTH_FORMAT, DATE_SEP, YEAR_LONG_FORMAT)

class TestResultBase(object):

  @property
  def login(self):
    return self.__login

  @property
  def status(self):
    return self.__status

  @property
  def date(self):
    return self.__date

  @property
  def mark(self):
    return self.__mark

  def __init__(self, *args):

    login, status, date, mark = args
    self.__login = self.parse_login(login)
    self.__status = self.parse_status(status)
    self.__date = self.parse_date(date)
    self.__mark = mark

  @abstractmethod
  def parse_mark(self):
    pass

  def parse_login(self, login):
    login = login.strip()
    if not login:
      raise TestResultException('Login cannot be empty')
    return login

  def parse_status(self, status):
    status = status.strip()
    if not status:
      raise TestResultException('Status cannot be empty')
    return status

  def parse_date(self, date):
    try:
      date = datetime.strptime(date, DATE_FORMAT)
    except ValueError:
      raise TestResultException('Date cannot parsed in current format')
    return date

  def __str__(self):
    props = [self.__login, self.__status, self.__date, self.__mark]
    return ';'.join(map(lambda x: str(x), props))

  def __repr__(self):
    return "%s;%s" % (self.__class__.__name__, self.__str__())



if __name__ == '__main__':
  test1 = TestResultBase('login', 'status', '21/12/2009')
  test(test1,'TestResultBase;login;status;2009-12-21 00:00:00;0')
