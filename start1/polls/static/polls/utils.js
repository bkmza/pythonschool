var utils = new function(){

  this.doAjax = function(url, data, callback){
    $.ajax({
      type: 'POST',
      url: url,
      dataType : "json",
      data: data,
      traditional: true,
      success: function (data) {
        callback(data);
      }
    });
  }

}();
