from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    text = models.TextField()
    date = models.DateTimeField()
    done = models.BooleanField()
    user = models.ForeignKey(User)