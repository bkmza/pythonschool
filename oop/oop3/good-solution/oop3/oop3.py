import csv
from purchases import *
from csv_exceptions import *


_PURCHASES_CLASSES = dict((cls.__name__, cls) for cls in [Purchase, PricePurchase, BulkPurchase])

def get_purchase_from_factory(rec):
    try:
        cls_obj = _PURCHASES_CLASSES[rec[0]]
        return cls_obj(*rec[1:])
    except KeyError:  
        raise CsvLineException('Wrong class name: ' + rec[0])
    except TypeError:  
        raise CsvLineException('Wrong number of arguments')


def get_purchases(filename):
    purchases = []
    with open(filename, 'r') as input_file:
        for rec in csv.reader(input_file, delimiter=';'):
            if not len(rec):
                continue
            try:
                purchase = get_purchase_from_factory(rec)
                purchases.append(purchase)
            except CsvLineException as e:
                print ('%-45s >> %s') % (';'.join(rec), e) 
        return purchases


def get_report(purchases):
    if not purchases:
        return 'Empty purchases list'

    columns = [
        ('Class name', 15, '<'),
        ('Commodity', len('Commodity'), '<'),
        ('Price', 12, '>'),
        ('Number', len('Number'), '>'),
        ('Discount', len('Discount'), '>'),
        ('Cost', 12, '>')
    ]

    table_len = sum(column[1] for column in columns) + len(columns) - 1
    table_sep_line = '-' * table_len
    table_header = '|'.join('{0:^{1}}'.format(column[0], column[1]) for column in columns)
    lines = ['']
    lines.append('{0:^{1}}'.format('Purchases list', table_len))
    lines.append(table_sep_line)
    lines.append(table_header)
    lines.append(table_sep_line)
    for purchase in purchases:
        line = '|'.join('{0:{1}{2}}'.format(item[0], item[1][2], item[1][1]) for item in zip(purchase.table_values(), columns))
        lines.append(line)
    total_cost = sum(purchase.get_cost() for purchase in purchases)
    table_footer = 'Total{0:{1}}'.format(total_cost, table_len - len('Total')) 
    lines.append(table_sep_line)
    lines.append(table_footer)
    return '\n'.join(lines)
         

def main(filename):
    try:
        purchases = get_purchases(filename)
        print get_report(purchases)
    except IOError:
        print 'No input file'
    

if __name__ == '__main__':
    main('in_task2_fifth.csv')