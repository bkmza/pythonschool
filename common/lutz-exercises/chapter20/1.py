def get_arg(arg):
  return arg

# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# Calls the above functions with interesting inputs.
def main():
  test(get_arg('string'), 'string')
  test(get_arg(100), 100)
  test(get_arg([100, 200, 300]), [100, 200, 300])
  test(get_arg({'a':1, 'b':2, 'c':3}), {'a':1, 'b':2, 'c':3})
  # test(print_arg(), 'some not excpected result')
  # test(print_arg(1,2), 'some not excpected result')

if __name__ == '__main__':
  main()
