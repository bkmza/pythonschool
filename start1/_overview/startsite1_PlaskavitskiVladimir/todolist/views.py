from django import forms
from django.utils import timezone
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.decorators.csrf import csrf_protect
from todolist.models import Task
import json
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class TasksView(generic.ListView):
    model = Task
    template_name = 'todolist/tasks.html'

    def __init__(self, header="Tasks", ex_menu=[], ex_commands=[]):
        self.menu = ["today", "someday", "fixed"]
        self.label_menu = {"today": "Today", "someday": "Someday", "fixed": "Fixed"}
        self.commands = ["add_task", "fix_task", "remove_task"]
        self.label_commands = {"add_task": "Add task", "fix_task": "Fix", "remove_task": "Remove"}
        self.header = header
        self.cut_links(ex_menu, self.menu)
        self.cut_links(ex_commands, self.commands)

    def cut_links(self, ex_links, links):
        for link in ex_links:
            if link in links:
                links.remove(link)

    def get_context_data(self, **kwargs):
        context = super(TasksView, self).get_context_data(**kwargs)
        if not context['object_list']:
            self.cut_links(["remove_task", "fix_task"], self.commands)
        context["header"] = self.header
        context["links"] = map(lambda x: ["todolist:" + x, self.label_menu[x]], self.menu)
        context["commands"] = map(lambda x: ["todolist:" + x, x, self.label_commands[x]], self.commands)
        context['user'] = self.request.user.username
        return context

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user.id)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(generic.ListView, self).dispatch(request, *args, **kwargs)


class TodayView(TasksView):
    def __init__(self):
        header = "Today "+timezone.now().strftime("%d:%m")
        TasksView.__init__(self, header, ["today"])

    def get_queryset(self):
        return TasksView.get_queryset(self).filter(date__lte=timezone.now()).filter(done=False).order_by('date')


class SomedayView(TasksView):
    def __init__(self):
        TasksView.__init__(self, "Someday", ["someday"])

    def get_queryset(self):
        return TasksView.get_queryset(self).filter(date__gt=timezone.now()).filter(done=False).order_by('date')


class FixedView(TasksView):
    def __init__(self):
        TasksView.__init__(self, "Fixed", ["fixed"], ["add_task", "fix_task"])

    def get_queryset(self):
        return TasksView.get_queryset(self).filter(done=True).order_by('-date')


class TaskForm(forms.Form):
    task = forms.CharField()
    date = forms.DateField()


class AddTaskFormView(generic.FormView):
    success_url = 'todolist:add_task'
    form_class = TaskForm
    template_name = 'todolist/add_task.html'
    model = Task


    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(generic.FormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        data = form.cleaned_data
        Task.objects.create(text=data["task"], date=data["date"], done=False, user=self.request.user)
        return redirect(self.get_success_url())


def process_tasks(request, process):
    if request.method == 'POST':
        ids = json.loads(request.POST["ids"])
        process(Task.objects.filter(pk__in=ids, user=request.user))
        #return HttpResponse("ok")
        return HttpResponseRedirect(reverse('todolist:tasks'))


@csrf_protect
def remove_task(request):
    return process_tasks(request, lambda x: x.delete())


@csrf_protect
def fix_task(request):
    return process_tasks(request, lambda x: x.update(done=True))