from django.conf.urls import patterns, url
from todolist import views

urlpatterns = patterns('',
    url(r'^today/$', views.TodayView.as_view(), name='today'),
    url(r'^someday/$', views.SomedayView.as_view(), name='someday'),
    url(r'^fixed/$', views.FixedView.as_view(), name='fixed'),
    url(r'^tasks/$', views.TasksView.as_view(), name='tasks'),
    url(r'^add_task/$', views.AddTaskFormView.as_view(), name="add_task"),
    url(r'^fix_task/$', views.fix_task, name="fix_task"),
    url(r'^remove_task/$', views.remove_task, name="remove_task"),
)