import ConfigParser
import re

SECTION_NAME = 'DEFAULT'
INDEX_KEY = 0
INDEX_VALUE = 1

def process(file_name):
 
  tuples = get_tuples_from_ini(file_name)
  if not len(tuples):
    return 'Data not found'

  values = {}   # contains key - ij, value - float value
  indexes = []  # contains ij indexes
  errors = 0    # errors count
  result = 0    # sum of values
  
  for tuple in tuples:
    m_i = re.search(r'^index(?P<i_val>.+)', tuple[INDEX_KEY])
    if m_i:
      i = m_i.group('i_val')
      j = tuple[INDEX_VALUE]
      if is_natural(i) and is_natural(j):
        index = i + j
        indexes.append(index)
      else:
        errors += 1
   
    m_v = re.search(r'^value(?P<ij_val>.+)', tuple[INDEX_KEY])
    if m_v:
      ij = m_v.group('ij_val')
      if is_natural(ij):
        values[ij] = tuple[INDEX_VALUE]
      else:
        errors += 1

  for index in indexes:
    try:
      i_index = int(index)
      result += float(values[index])
    except:
      errors += 1
  
  return 'result = %s errors = %d' % ('{0:g}'.format(result), errors)

def is_natural(value):
  m_nat = re.search(r'^[1-9]\d*', value.strip())
  if not m_nat:
    return False
  return True
                   
def get_tuples_from_ini(file_name):
  Config = ConfigParser.ConfigParser()
  Config.read(file_name)
  
  # Access to element by key
  #print Config.get(SECTION_NAME, 'index1')
  #print Config.sections()
  #print Config.items('WINTERISCOMING')
  #print Config.get('WINTERISCOMING', 'value52')
  #print Config.items('DEFAULT')
  #print Config.get('WINTERISCOMING', 'value52')
  
  return Config.items('DEFAULT')

               
               
               
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'strings2'
  #test(process('test0.ini'), 'Data not found')
  test(process('test1.ini'), '''result = 8.24 errors = 3''')
  #test(process('test2.ini'), '''result = 30.242 errors = 9''')

if __name__ == '__main__':
  main()

