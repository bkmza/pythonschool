import random

from Person import Person
from shipments.Solid import Solid
from shipments.Liquid import Liquid
from shipments.Amorphous import Amorphous


class Life():

    @staticmethod
    def get_case(count=5, variants=[Person, Solid, Liquid, Amorphous]):
        return [random.choice(variants)() for _ in xrange(count)]