from purchaseexception import CommodityException
from utils import test

class Commodity(object):
  
  @property
  def name(self):
    return self.__name
  
  @property
  def price(self):
    return self.__price

  def __init__(self, name, price):
    name = name.strip()
    if not name:
      raise CommodityException(0, 'Name cannot be empty')

    try:
      int_price = int(price)
    except ValueError:
      raise CommodityException(0, 'Cannot convert price to int base 10')

    if int_price <= 0:
      raise CommodityException(0, 'Price cannot be less or equal zero')

    self.__name = name
    self.__price = int_price
  
  def __str__(self):
    props = [self.name, self.price]
    return ';'.join(map(lambda x: str(x), props))
 
  def __eq__(self, that):
    return self.name == that.name and self.price == that.price
  
  def __hash__(self):
    return hash(self.name, self.price)
  
  def __getitem__(self, key):
    val = dict.__getitem__(self, key)
    return val
  
  def __repr__(self):
    return "%s;%s;%s" % (self.__class__.__name__, self.name, self.price)
  
if __name__ == '__main__':
  c1 = Commodity('Snikers', price=5)
  c2 = Commodity('Mars', price=4)
  print c1.__class__.__name__
  test(c1.__str__(), 'Snikers;5')
  test(c2.__str__(), 'Mars;4')
