#!/usr/bin/python

from math import pi


class FerryTransportItem(object):
    """
    FerryTransportItem
    Interface - like class that represents method's which must be implemented in classes
     to ferry can transport them

    brutto_weight - property, which returns full weight of item.
    """

    @property
    def brutto_weight(self):
        raise NotImplementedError

    def __str__(self):
        raise NotImplementedError


class Ferry(object):
    """
    Ferry representation class
    ORDERING - dictionary for ordering transportation list by type of items
    """

    ORDERING = {
        "Passenger": 1,
        "ContainerCargo": 2,
        "Cargo": 3,
        "TankCargo": 4,
    }
    
    def __init__(self, capacity):
        self.__capacity = capacity
        self.__transportation_list = []

    def add_to_transportation_list(self, item):
        """
        Ads one item to transportation list if it can be transported
        """
        if isinstance(item, FerryTransportItem):
            self.__transportation_list.append(item)
        else:
            print "Sorry, ferry can't transport this type of item"

    def show_transportation_list(self):
        """
        Prints current transportation list
        """
        for item in self.__transportation_list:
            print item
        print "Summary list weight: {0}".format(self.full_cargo_weight())

    def order_transportation_list(self):
        """
        Orders transportation list by item type in order defined in ORDERING dictionary
        """
        self.__transportation_list.sort(key = lambda i: Ferry.ORDERING[i.__class__.__name__])

    def full_cargo_weight(self):
        """
        Returns summary weight of items in transportation list
        """
        return sum(item.brutto_weight for item in self.__transportation_list)

    def _is_ferry_overloaded(self):
        """
        Inner method which defines is Ferry overloaded
        """
        if self.__capacity < self.full_cargo_weight():
            return True
        return False

    def can_ferry_transport_items(self):
        """
        Outer method to show can transportation list items be transported
        """
        if self._is_ferry_overloaded():
            print "Ferry can't transport all items from this list, they are to heavy"
        else:
            print "Ferry can do the job!"


class Cargo(FerryTransportItem):
    """
    Represents simple cargo, which transports without additional container or tank
    """

    def __init__(self, cargo_weight):
        self.__cargo_weight = cargo_weight

    @property
    def brutto_weight(self):
        return self.__cargo_weight

    def __str__(self):
        return "{0};{1}".format(self.__class__.__name__, self.brutto_weight)


class ContainerCargo(Cargo):
    """
    Represents cargo, which transports in container
    """

    def __init__(self, cargo_density, container_width=2, container_length=6,
                                container_height=2, container_weigth=2300):
        self.__cargo_density = cargo_density
        self.__container_width = container_width
        self.__container_length = container_length
        self.__container_height = container_height
        self.__container_weight = container_weigth

    @property
    def cargo_weight(self):
        return (self.__container_width * self.__container_length * self.__container_height *
                self.__cargo_density)

    @property
    def brutto_weight(self):
        return self.cargo_weight + self.__container_weight


class TankCargo(Cargo):
    """
    Represents cargo, which transports in tank
    """

    def __init__(self, cargo_density, tank_radius=1, tank_height=6, tank_weight=3000):
        self.__cargo_density = cargo_density
        self.__tank_radius = tank_radius
        self.__tank_height = tank_height
        self.__tank_weight = tank_weight

    @property
    def cargo_weight(self):
        return int(round(pi * (self.__tank_radius ** 2) * self.__tank_height *
                         self.__cargo_density))

    @property
    def brutto_weight(self):
        return self.cargo_weight + self.__tank_weight


class Passenger(FerryTransportItem):
    """
    Represents passenger
    """

    def __init__(self, name, weight):
        self.__name = name
        self.__weight = weight

    @property
    def brutto_weight(self):
        return self.__weight

    def __str__(self):
        return "{0};{1};{2}".format(self.__class__.__name__, self.__name, self.brutto_weight)


def main():
    ferry = Ferry(50000)
    ferry.add_to_transportation_list(Cargo(1000))
    ferry.add_to_transportation_list(ContainerCargo(1000))
    ferry.add_to_transportation_list(ContainerCargo(800, 3, 8, 2, 3000))
    ferry.add_to_transportation_list(TankCargo(1000))
    ferry.add_to_transportation_list(Passenger("Bob", 87))
    ferry.add_to_transportation_list(Passenger("Alice", 55))
    ferry.add_to_transportation_list(Passenger("Eva", 67))
    ferry.add_to_transportation_list(Passenger("Mike", 101))

    print "Transportation list:"
    ferry.show_transportation_list()
    ferry.order_transportation_list()
    print '*' * 20

    print "\nOrdered transportation list:"
    ferry.show_transportation_list()
    print '*' * 20
    print
    ferry.can_ferry_transport_items()


if __name__ == "__main__":
    main()
