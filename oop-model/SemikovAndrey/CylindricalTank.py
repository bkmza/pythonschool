__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from Cargo import Cargo
import math


class CylindricalTank(Cargo):

    def __init__(
            self,
            name="Cylindrical Tank",
            density=950,
            inner_diameter=2.5,
            inner_length=5.887,
            self_weight=2*1000
    ):
        self._density = density
        self._inner_diameter = inner_diameter
        self._inner_length = inner_length
        self._self_weight = self_weight
        super(CylindricalTank, self).__init__(name)

    def _calculate_weight(self):
        return self._self_weight + self._inner_length * ((math.pi * math.pow(self._inner_diameter, 2)) / 4)
