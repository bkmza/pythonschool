from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from polls import views

urlpatterns = patterns('',
    # ex: /polls/
    url(r'^$', RedirectView.as_view(url='polls/list/today')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/polls/accounts/login'}),
    (r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/')),
    url(r'^accounts/users/', RedirectView.as_view(url='/polls/accounts/login')),

    url(r'^$', views.login, name='login'),
    url(r'^list/(?P<type>\w+)$', views.list, name='list'),
    url(r'^add-task/$', views.addtask, name='addtask'),
    url(r'^remove$', views.remove, name='remove'),
    url(r'^fix$', views.fix, name='fix'),
)