class Purchase(object):
    
    @property
    def name(self):
        return self.__name
        
    @property
    def price(self):
        return self.__price    
        
    @property
    def qty(self):
        return self.__qty    
   
    def __init__(self, name, price, qty): 
        self.__name  = name
        self.__price = price
        self.__qty   = qty        
    
    def __eq__(self, other):
        return self.name == other.name and self.price == other.price   
    
    def __str__(self):
        return "{0};{1};{2};{3}".format(self.name, self.price, self.qty, self.get_cost())
        
    # TODO(Vova): cached in the future   
    def get_cost(self):        
        return self.price * self.qty