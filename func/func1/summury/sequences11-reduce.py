import sys
import math
import re


def get_segment_len(line, re_coords=re.compile(r'[\s();]+')):
    coords = re_coords.split(line)[1:-1]
# initial and last elements are discarded    
    diff_x = float(coords[2]) - float(coords[0])
    diff_y = float(coords[3]) - float(coords[1])
    return  int(math.hypot(diff_x, diff_y) + 0.5)


def get_lines(filename):
    with open(filename, 'r') as f:
        for line in f:
            yield line


def get_appended_unique_lengths(unique_lengths, item):
     if item not in unique_lengths:
         unique_lengths.append(item)
     return unique_lengths


def main(filename):
    try:
        lines = get_lines(filename)
        f_segment_lengths = (get_segment_len(line) for line in lines)
        print sorted(reduce(get_appended_unique_lengths, f_segment_lengths, []))
    except IOError:
        print 'wrong filename'


if __name__ == '__main__':
    main(sys.argv[1])
