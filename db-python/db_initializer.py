import sys
import sqlite3


def create_int_marks(db_path):
  conn = sqlite3.connect(db_path)
  c = conn.cursor()

  c.execute('drop table if exists logins')
  c.execute('drop table if exists tests')
  c.execute('drop table if exists results')

  c.executescript('''
        create table logins (
            id integer primary key autoincrement,
            name text
        );

        create table tests (
            id integer primary key autoincrement,
            name text
        );

        create table results (
            loginId integer,
            testId integer,
            date timestamp,
            mark integer,
            foreign key (loginId) references logins (id),
            foreign key (testId) references tests (id)
        );
        ''')
  conn.commit()
  conn.close()

def create_real_marks(db_path):
  conn = sqlite3.connect(db_path)
  c = conn.cursor()

  c.execute('drop table if exists logins')
  c.execute('drop table if exists tests')
  c.execute('drop table if exists results')

  c.executescript('''
        create table logins (
            id integer primary key autoincrement,
            name text
        );

        create table tests (
            id integer primary key autoincrement,
            name text
        );

        create table results (
            loginId integer,
            testId integer,
            date timestamp,
            mark real,
            foreign key (loginId) references logins (id),
            foreign key (testId) references tests (id)
        );
        ''')
  conn.commit()
  conn.close()


def main():
  if len(sys.argv) != 3:
    print 'usage: ./db_initializer.py {--type} {--dbpath}'
    sys.exit(1)

  type, db_path = sys.argv[1:]
  if type == 1:
    create_int_marks(db_path)
  if type == 2:
    create_real_marks(db_path)


if __name__ == '__main__':
  main()