from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from django.utils import timezone
import json
from django.contrib.auth.decorators import login_required

from polls.forms import TaskForm
from polls.models import Task


def login(request):
  return render(request, 'polls/login.html', {})

def index(request):
    context = {}
    return render(request, 'polls/list.html', context)


@login_required
@require_http_methods(["GET"])
def list(request, type):
    now = timezone.now()
    tasks = []
    if type == 'today':
      tasks = Task.objects.filter(date__lte=now,is_fixed=False,user_id=request.user.id)
    if type == 'someday':
      tasks = Task.objects.filter(date__gt=now,is_fixed=False,user_id=request.user.id)
    if type == 'fixed':
      tasks = Task.objects.filter(is_fixed=True,user_id=request.user.id)

    tasks.order_by('-date')
    context = {'tasks_list': tasks, 'type': type }
    return render(request, 'polls/list.html', context)


@login_required
@require_http_methods(["GET", "POST"])
def addtask(request):

  if request.method == 'POST':
    try:
      form = TaskForm(request.POST)
      if not form.is_valid():
        return render(request, 'polls/addtask.html', {'form':form, 'message':'Form is invalid'})
      f = form.save(commit=False)
      f.user = request.user
      f.save()
      type = form.cleaned_data['type']
    except ValueError as e:
      return render(request, 'polls/addtask.html', {'message':e})

    return HttpResponseRedirect('/polls/list/{0}'.format(type))
  else:
    type = request.GET.get('type')
    if not type:
      type = 'today'
    form = TaskForm(initial={'type': type})
    return render(request, 'polls/addtask.html', {'form': form, 'type': type})

@login_required
@require_http_methods(["POST"])
def remove(request):
    ids = request.POST.getlist('ids')
    map(lambda id: Task.objects.filter(id=id).delete(), ids)
    return HttpResponse(json.dumps({'is_success':True, 'ids': ids}), content_type="application/json")


@login_required
@require_http_methods(["POST"])
def fix(request):
    ids = request.POST.getlist('ids')
    map(lambda id: Task.objects.filter(id=id).update(is_fixed=True), ids)
    return HttpResponse(json.dumps({'is_success':True, 'ids': ids}), content_type="application/json")
