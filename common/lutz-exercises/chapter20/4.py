def adder(**kargs):
  keys = kargs.keys()
  sum = kargs[keys[0]]
  for key in keys[1:]:
    sum += kargs[key]
  return sum
 
print adder(good=1, bad=2, ugly=8)
print(adder(a=1, b=2, c=3), adder(a='aa', b='bb', c='cc'))
