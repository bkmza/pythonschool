from utils import test
from purchase import Purchase
from purchaseexception import PricePurchaseException
  
class PricePurchase(Purchase):
  
  @property
  def discount(self):
    return self.__discount
  
  def __init__(self, *args):
    if len(args) > 4:
      raise PricePurchaseException(0, 'Too many agruments')
    if len(args) <= 3:
      raise PricePurchaseException(0, 'Too few agruments')

    name, price, quantity, discount = args

    if not discount:
      raise PricePurchaseException(0, 'Discount should exist')

    try:
      int_discount = int(discount)
    except ValueError:
      raise PricePurchaseException(0, 'Cannot convert discount to int base 10')

    Purchase.__init__(self, name, price, quantity)
    self.__discount = int_discount
    
  def get_cost(self):
    return self.commodity.price * self.quantity - self.discount * self.quantity
  
  def __str__(self):
    return Purchase.__str__(self) + ';%s;%s' % (self.discount, self.get_cost())
    
    
if __name__ == '__main__':
  pp1 = PricePurchase('Beer', price=300, count=2, discount=50)
  pp2 = PricePurchase('Beer', price=300, count=2, discount=50)
  print pp1.__class__.__name__
  test(pp1.__str__(), 'Beer;300;2;50;500')
  test(pp2.__str__(), 'Beer;300;2;50;500')
  test(pp1.get_cost(), 500)
  test(pp2.get_cost(), 500)
  test(pp1 == pp2, True)
  test(pp2.__eq__(pp1), True)
   