from django.contrib import admin
 
from start.models import Task
 
class TaskAdmin(admin.ModelAdmin):
    list_display = ['user', 'content', 'fix_date', 'is_fixed']
 
admin.site.register(Task, TaskAdmin)
