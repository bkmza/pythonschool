from abc import ABCMeta
from random import randint


class SortableGoods(object):
  __metaclass__ = ABCMeta

  _GOODS_CLASSES_PRIORITY = {
    'Person': 0,
    'Container': 1,
    'Platform': 2,
    'Cylinder': 3
  }

  _max_priority = len(_GOODS_CLASSES_PRIORITY) + 1

  def __eq__(self, other):
    return self._priority(self) == self._priority(other)

  def __ne__(self, other):
    return self._priority(self) != self._priority(other)

  def __lt__(self, other):
    return self._priority(self) < self._priority(other)

  def __le__(self, other):
    return self._priority(self) <= self._priority(other)

  def __gt__(self, other):
    return self._priority(self) > self._priority(other)

  def __ge__(self, other):
    return self._priority(self) >= self._priority(other)

  def _priority(self, target):
    return self._GOODS_CLASSES_PRIORITY.get(target.__class__.__name__, self._max_priority)


class WeightCalculation(object):
  __metaclass__ = ABCMeta

  def __init__(self, square, height, density, container_weight):
    self._square = square
    self._height = height
    self._density = density
    self._container_weight = container_weight

  def weight(self):
    return self._square * self._height * self._density + self._container_weight

  def __str__(self):
    weight = 'x'.join([str(self._square), str(self._height), str(self._density)])
    return '%s: %s + %s' % (self.__class__.__name__, weight, self._container_weight)


class OwnWeight(object):
  __metaclass__ = ABCMeta

  def __init__(self, weight):
    self._weight = weight

  def weight(self):
    return self._weight

  def __str__(self):
    return '%s: %s' % (self.__class__.__name__, self._weight)


class Container(WeightCalculation, SortableGoods): pass


class Cylinder(WeightCalculation, SortableGoods): pass


class Platform(OwnWeight, SortableGoods): pass


class Person(OwnWeight, SortableGoods): pass


class Ferry(object):
  def __init__(self, capacity):
    self._capacity = capacity
    self._goods = []

  @property
  def goods(self):
    return self._goods

  def put(self, product):
    self._goods.append(product)

  def is_overloaded(self):
    return sum([g.weight() for g in self._goods]) > self._capacity


ferry = Ferry(10000)
for i in range(100):
  ferry.put(Container(randint(1, 10), randint(1, 10), randint(1, 10), randint(100, 1000)))
  ferry.put(Cylinder(randint(1, 10), randint(1, 10), randint(1, 10), randint(100, 1000)))
  ferry.put(Platform(randint(1, 100)))
  ferry.put(Person(randint(1, 100)))

print 'Overloaded?: %s' % ferry.is_overloaded()
print [str(g) for g in sorted(ferry.goods)]
