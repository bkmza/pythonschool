/*function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}*/


$(document).ready(function() {
    $("#remove_task, #fix_task").click(function() {
        $.post($(this).attr("href"),
               {
                csrfmiddlewaretoken: $.cookie('csrftoken'),
                ids: JSON.stringify($('table input[type="checkbox"]:checked').map(function(){ return this.name; }).get())
               },
               function(data){
                   //alert(data);
                   location.reload();
               }
        );
        return false;
    });
});