__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from abc import ABCMeta
from abc import abstractmethod


class Cargo(object):

    __metaclass__ = ABCMeta

    def __init__(self, name):
        self._weight = self._calculate_weight()
        self._name = name

    @property
    def weight(self):
        return self._weight

    @property
    def name(self):
        return self._name

    @abstractmethod
    def _calculate_weight(self):
        """
        calculate cargo weight
        :return float
        """
        return

    def __str__(self):
        return "%s weights %d" % (self.name, self.weight)

    def __repr__(self):
        return self.__str__()