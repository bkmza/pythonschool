from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.views.generic import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^start/', include('start.urls', namespace="start")),
    url(r'^paranoia.admin/', include(admin.site.urls)),
)
