#!/usr/bin/env python

import re
import locale
import calendar


def remove_whitespace(s, _pat = re.compile(r'\s+')):
  """Remove all whitespace from given string.
     _pat argument should not be used by calling code!"""
  return re.sub(_pat, '', s)

def normalize_amounts(s, _pat = re.compile(r'\b([1-9]\d{0,2})((?:\s+\d{3})*)\s+(blr|belarusian roubles)\b')):
  """Process roubles money amounts in given string according to assignment description.
     _pat argument should not be used by calling code!"""
  def replacement(m):
    return m.group(1) + remove_whitespace(m.group(2)) + ' ' + m.group(3)
  return re.sub(_pat, replacement, s)


def to_abs_year(yy_or_yyyy_str):
  """Convert year string in format yy or yyyy to year in format yyyy"""
  value = int(yy_or_yyyy_str)
  return value if value >= 1000 else 2000 + value

def to_month_name(idx_str):
  """Return month name according to given position (1-12)"""
  return calendar.month_name[int(idx_str)]

def normalize_dates(s, _pat = re.compile(r'\b(\d{1,2})([\./-])(\d{1,2})\2((?:\d{2}){1,2})\b')):
  """Process dates in given string according to assignment description.
     _pat argument should not be used by calling code!"""
  def replacement(m):
    return '%s %s, %s' % (to_month_name(m.group(3)), m.group(1).zfill(2), to_abs_year(m.group(4)))
  return re.sub(_pat, replacement, s)


def process_lines(lines, *processing_fns):
  """Apply all given processing functions to each line from given lines
     list and return resulting sequence of lines."""
  for line in lines:
    processed = line
    for processing_fn in processing_fns:
      processed = processing_fn(processed)
    yield processed

def converted_file_content(filepath):
  """Process all lines from given file with two 'normalize' functions defined above
     and return new file content as a single string"""
  with open(filepath, 'r') as f:
    lines = process_lines(f.readlines(), normalize_dates, normalize_amounts)
    return ''.join(lines)


def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))

def main():
  # ensure we have English locale, need this for month names
  locale.setlocale(locale.LC_ALL, 'en_US')

  print 'Test remove_whitespace'
  test(remove_whitespace(' asdf qwe   123 '), 'asdfqwe123')

  print
  print 'Test process_lines'
  test(list(process_lines(['line1', 'line2'], lambda s: s + 'a', lambda s: s + 'b')),
       ['line1ab', 'line2ab'])

  print
  print 'Test normalize_amounts'
  amount_tests = [
    ('100   500 belarusian roubles vs 1 2 3 600 blr',
     '100500 belarusian roubles vs 1 2 3600 blr'),
    ('010 123 300 blr,  10  300 blr.',
     '010 123300 blr,  10300 blr.'),
    ('10 blr, 100 blr, 12 1000 blr, 121 000 blr, 1 belarusian roubles',
     '10 blr, 100 blr, 12 1000 blr, 121000 blr, 1 belarusian roubles'),
    ('123 1 blr, 123 01 blr, 123 10 blr, 123 100 blr',
     '123 1 blr, 123 01 blr, 123 10 blr, 123100 blr'),
    ('123456 123 456 blr',
     '123456 123456 blr'),
    ('123 456 blrq and 123 456 belarusian roublesq vs 123 456 blr.',
     '123 456 blrq and 123 456 belarusian roublesq vs 123456 blr.'),
    ('asdf123 456 789 012 345 blr',
     'asdf123 456789012345 blr'),
    ('123 456blr',
     '123 456blr')]
  for data, result in amount_tests:
    test(normalize_amounts(data), result)

  print
  print 'Test to_abs_year'
  test(to_abs_year('13'), 2013)
  test(to_abs_year('1300'), 1300)

  print
  print 'Test to_month_name'
  test(to_month_name('12'), 'December')
  test(to_month_name('1'), 'January')
  test(to_month_name('02'), 'February')  

  print
  print 'Test normalize_dates'
  date_tests = [
    ('10.10.10 vs 10.10/10 or 10/10.10', 'October 10, 2010 vs 10.10/10 or 10/10.10'),
    ('10.10.10q', '10.10.10q'),
    ('01.01.201', '01.01.201'),
    ('01.01.2011', 'January 01, 2011'),
    ('1.1.2011', 'January 01, 2011'),
    ('01.02.03 vs 01/02/03 vs 01-02-03', 'February 01, 2003 vs February 01, 2003 vs February 01, 2003'),
    ('123.02.03', '123.02.03')]
  for data, result in date_tests:
    test(normalize_dates(data), result)

  print
  print 'Test complete file processing'
  with open('output.txt') as expected_f:
    test(converted_file_content('input.txt'), expected_f.read()) 

if __name__ == '__main__':
  main()
