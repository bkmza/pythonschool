class Purchase(object):

    def __init__(self, name, price, number):
        self._name = name.strip()
        if not self._name:
            raise EmptyCsvLineException('purchase name')
        self._price = Purchase.get_converted_positive_value(int, price, 'price')
        self._number = Purchase.get_converted_positive_value(int, number, 'number')
        
    def get_cost(self):
        return self._price * self._number

    def _fields_values(self):
        class_name = self.__class__.__name__
        return [class_name, self._name, self._price, self._number]
    
    def table_values(self):
        return self._fields_values() + ['', self.get_cost()]

    def __str__(self):
        return ';'.join([str(x) for x in self._fields_values() + [self.get_cost()]])

    def __eq__(self, other):
        return isinstance(other, Purchase) and self._name == other._name and self._price == other._price

    @staticmethod
    def get_converted_positive_value(conv_function, str_value, field_name):
        try:
            value = conv_function(str_value)
            Purchase.check_positive(value, field_name)
            return value
        except ValueError:
            raise ValueCsvLineException(field_name)
      
    @staticmethod
    def check_positive(value, field_name):
        if value <= 0:
            raise NonpositiveCsvLineException(field_name)
      

class PricePurchase(Purchase):

    def __init__(self, name, price, number, discount):
        Purchase.__init__(self, name, price, number)
        self._discount = Purchase.get_converted_positive_value(int, discount, 'discount')
        Purchase.check_positive(self._price - self._discount, 'price after discount')
        
    def get_cost(self):
        return (self._price - self._discount) * self._number
    
    def _fields_values(self):
        return super(PricePurchase, self)._fields_values() + [self._discount]

    def table_values(self):
        return self._fields_values() + [self.get_cost()]


class BulkPurchase(Purchase):
    __BULK_NUMBER = 100

    def __init__(self, name, price, number, discount):
        Purchase.__init__(self, name, price, number)
        self._discount = Purchase.get_converted_positive_value(float, discount, 'discount')
        
    def get_cost(self):
        cost = super(BulkPurchase, self).get_cost()
        if self._number > BulkPurchase.__BULK_NUMBER:
            cost *= (1 - self._discount/100) 
        return int(cost + 0.5)
    
    def _fields_values(self):
        return super(BulkPurchase, self)._fields_values() + [self._discount]

    def table_values(self):
        return self._fields_values() + [self.get_cost()]


