from django import forms
from start.models import Task


class AddTaskForm(forms.Form):
    content = forms.CharField(
        label='Content',
        widget=forms.TextInput(attrs={'size': 64})
    )
    fix_date = forms.DateField(
        label='Fixed date',
        widget=forms.DateInput
    )
    
class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('content', 'fix_date')
        labels = {'fix_date': 'Fixed date',}
