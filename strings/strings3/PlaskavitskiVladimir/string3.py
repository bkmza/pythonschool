#encoding:UTF-8
import re
import datetime

GROUP_SEARCH = "search" # вместо группы можно было обрамить ненужные части выражения префиксную (?<=...) и постфиксную (?=...) проверку, но это всегда можно успеть :)
SEPARETE_DATE = ".|/-"
SEPARATOR_MONEY = " " # символы, которые будут отделять численную величину денег от наименования
LIST_MONEY_NAMES = ["belarusian roubles","blr"]

def replacer_money(m):
    return re.sub("\s", "", m.group(GROUP_SEARCH)) + SEPARATOR_MONEY 

def re_money():
    return re.compile(r"\b"
                       "(?P<"+GROUP_SEARCH+">" # область для последующей обработки
                           "\d{1,3}"           # цифры первого разряда (может быть не полным)
                           "(?:\s*\d{3})*"     # дальше допускаются только наборы по три цифры, разделенные произвольным количеством пробелом
                           "\s+"               # пробельные символы между числом и наименованием денежной единицы
                       ")"                          
                       "(?="+"|".join(LIST_MONEY_NAMES)+")" # возможные наименования денег
                       )    


def re_date():
    return re.compile(r"\b"                    
                       "(?P<"+GROUP_SEARCH+">"             # область для последующей обработки
                           "(?P<day>0?[1-9]|[1-2]\d|3[0-1])"    # день 1-9 (либо 01-09),10-29,30-31
                           "(["+SEPARETE_DATE+"])"         # список допустимых разделителей
                           "(?P<month>0?[1-9]|1[0-2])"            # месяц 1-9 (либо 01-09), 10-12
                           r"\3"                           # тот же разделитель, что и до этого 
                           "(?P<year>\d{2}|[1-2]\d{3})"          # год 00-99, 1000-2999 (в 3000 упадут даты в Fortran и настанет конец света, а значит нет смысла их указывать) 
                       ")"                                     
                       r"\b"
                      )    

def replacer_date(m):
    
    day = int(m.group("day"))
    month = int(m.group("month"))
    year = int(m.group("year"))
    
    """
        Дополнительная мудрость:
            Если указан год в сокращенном виде, то попробовать угадать столетие
            
            Пример: 99 скорее всего 1999, а не 2099, т.к. 99 намного больше чем 13 (на данный момент 2013 год).
            Пример: 15 скорее всего 2015, а не 1915, т.к. 15 не намного больше чем 13
            
            Решение: определить к какому столетию ближе,
                     учитывая что из прошлого могту быть указаны любые даты, 
                     а в будущее больше чем на MAX_PERSPECTIVE лет не заглядывают
    
    """
    if(year<100):
                
        now_time = datetime.datetime.now()
        current_year = int(now_time.strftime("%y"))
        centuries = int(now_time.strftime("%Y")) - current_year
        range = current_year-year  
        
        MAX_PERSPECTIVE = 10 # максимальная разница между текущим годом и годом из будущего
        
        century = 100;        
          
        if range < 0:
            if century/2-year+current_year <= 0 or -range > MAX_PERSPECTIVE: # приоритет вчерашнему дню
                centuries -= century
        elif range > 0:
            if century/2 + year - current_year < 0 and range < MAX_PERSPECTIVE:
                centuries += century
        
        year+=centuries
        
        
    """ 
        На всякий случай обезопасить преобразование даты
        вдруг кто-нибудь указал 29 февраля 2013 (не високосный год) 
        либо 31 сентября (30 сентября последний день)    
    """    
    
    try:   
        return datetime.date(year, month, day).strftime("%B %d, %Y")
    except:
        return m.group(0)  
         

def change_text(text, re_custom, replacer):        
    return re_custom.sub(replacer,text)
    

def process_text(path_input):
        
    text = ""    

    try:
        with open(path_input, 'r') as f:
            text = f.read()
    except:
        print 'cannot open "%s"' % path_input
        return ""

    text = change_text(text, re_money(), replacer_money)    
    text = change_text(text, re_date(), replacer_date)  
    
    return text      
    


def test(result, path_pattern):    
    try:
        with open(path_pattern, 'rU') as f:
            pattern = f.read()            
            #print pattern+"\n\n"+result+"\n"                
            return pattern == result                        
    except:
        #print 'cannot open "%s"' % pathPattern
        return ""        
    
    return False    

def main():
    for i in range(1,6):
        path_input = "tests/input%d.txt" % i
        path_pattern = "tests/pattern%d.txt" % i
        result = process_text(path_input)
        valid = test(result, path_pattern)
        print "test {0} - {1}".format(i, valid)
        
if __name__ == '__main__':
    main()