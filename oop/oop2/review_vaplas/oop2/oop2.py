import csv
import purchaseclasses

from operator import *
from Commodity import Commodity


def get_purchases(filename):
    with open(filename, 'r') as input_file:
        for rec in csv.reader(input_file, delimiter=';'):
            yield get_class_from_factory(convert_record(rec))


def convert_record(rec):
    cls_type = rec[0]
    name = rec[1]
    price = int(rec[2])
    qty = int(rec[3])
    discount = float(rec[4])
    return cls_type, name, price, qty, discount


def get_class_from_factory((cls_type, name, price, qty, discount)):
    commodity = Commodity(name, price)
    purchase = getattr(purchaseclasses, cls_type)(commodity, qty, discount)
    return commodity, purchase


def get_info(filename):
    info = {}
    for commodity, purchase in get_purchases(filename):
        if commodity not in info:
            info[commodity] = []
        info[commodity].append(purchase)
    return info


def criterion_cost_items(obj):
    return sum(item.get_cost() for item in obj[1])


def criterion_count_items(obj):
    return len(obj[1])


def filter_by_price(key, items, oper, VALUE):
    return items if oper(key.price, VALUE) else []


def filter_by_cost(key, items, oper, VALUE):
    return [item for item in items if oper(item.get_cost(), VALUE)]


def general_filter(info, VALUE, oper, getter):
    return dict(filter(itemgetter(1), ((key, getter(key, value, oper, VALUE)) for key, value in info.iteritems())))

    #return dict(filter(itemgetter(1), dict((key, getter(key, value, oper, VALUE)) for key, value in info.iteritems()).iteritems()))

    #def get_iter():
    #    for key, value in info.iteritems():
    #        yield (key, getter(key, value, oper, VALUE))
    #return dict(filter(itemgetter(1), get_iter()))


#def general_filter(info, VALUE, oper, getter):
#    def is_leave(item):
#        return oper(getter(item), VALUE)
#    return dict(filter(itemgetter(1), dict((key, filter(is_leave, (key, value))) for key, value in info.iteritems()).iteritems()))


def general_sorted(info, *args):
    return sorted(info.iteritems(), key=lambda obj: tuple(criterion(obj)*(-1)**revers for criterion, revers in args))


def print_list(info):
    for key, value in info:
        print "{0} >> {1}".format(key, value)
    print


def proccess(filename):
    info = get_info(filename)
    print_list(info.iteritems())
    info = general_filter(info, 1000, ge, filter_by_price)
    info = general_filter(info, 5000, ge, filter_by_cost)
    seq = general_sorted(info, [criterion_count_items, False], [criterion_cost_items, True])
    print_list(seq)


def main():
    proccess("tests/in1.csv")


if __name__ == '__main__':
    main()