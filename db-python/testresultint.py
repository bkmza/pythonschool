from testresultbase import TestResultBase
from testresultexception import TestResultException

class TestResultInteger(TestResultBase):

  def __init__(self, *args):

    login, status, date, mark = args
    mark = self.parse_mark(mark)
    TestResultBase.__init__(self, login, status, date, mark)


  #task 1. only integer marks. range 0-10.
  def parse_mark(self, mark):
    try:
      mark = int(mark)
    except ValueError:
      raise TestResultException('Mark cannot parsed to int. Value = {0}'.format(mark))

    if mark > 10 or mark < 0:
      raise TestResultException('Mark cannot be less zero or greater 10. Value = {0}'.format(mark))

    return mark

  def __str__(self):
    return TestResultBase.__str__(self)

  def __repr__(self):
    return TestResultBase.__repr__(self)


if __name__ == '__main__':
  pass