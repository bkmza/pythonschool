from decorators import repr


@repr
class Amorphous():
    @property
    def weight(self):
        return self.__weight

    def __init__(self, weight=42):
        self.__weight = weight