from containers.Box import Box
from containers.Tube import Tube
from shipments.Solid import Solid
from shipments.Liquid import Liquid
from shipments.Amorphous import Amorphous
from Company import Company
from Person import Person

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))



def main():
    test(Box(None, 1, 1, 1, 1, 1).weight, 26)
    test(Box(None, 1, 1, 1, 1, 1).weight+Box(None, 1, 1, 3, 3, 3).weight, Box(Box(None, 1, 1, 1, 1, 1), 1, 1).weight)
    test(Liquid(2, 1000).weight, 2000)
    test(round(Tube(Liquid(1, 1000), 2000, 0.01).weight), 1113)
    test(round(Tube(Solid(1, 1, 1, 1000), 2000, 0.01).weight), 1096)
    test(Solid(1, 2, 1, 2000).weight, 4000)
    test(Box(Solid(1, 1, 1, 1000), 1, 1, 1, 1, 1).weight, 1026)
    test(Box(Box(None, 1, 1, 1, 1, 1), 1, 1).weight, 124)
    test(round(Tube(Tube(None, 1, 1, 1, 1), 1, 1).weight), 97)
    test(round(Box(Tube(None, 1, 1, 1, 1),1,1).weight), 118)
    test(round(Tube(Box(None, 1, 1, 1, 1, 1), 1, 1).weight), 103)
    test(str(Company.sorting([Box(), Tube(), Person(), Amorphous()], {"Person": 0, "Tube": 1, "Box": 2, "Amorphous": 3})), "[Person, Tube, Box, Amorphous]")
    test(str(Company.packing([Solid(), Liquid(), Amorphous(), Person(), Box(), Tube()])), "[Box, Tube, Amorphous, Person, Box, Tube]")

if __name__ == '__main__':
    main()