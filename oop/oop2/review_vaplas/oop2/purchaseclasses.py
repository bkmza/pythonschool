from abc import abstractmethod


class Purchase():

    @property
    def commodity(self):
        return self.__commodity

    @property
    def qty(self):
        return self.__qty    

    @abstractmethod
    def get_cost(self):
        pass

    def __init__(self, commodity, qty):
        self.__commodity = commodity
        self.__qty = qty

    def __str__(self):
        return "{0};{1};{2}".format(self.__class__.__name__, self.qty, self.get_cost())

    def __repr__(self):
        return self.__str__()


class PricePurchase(Purchase):

    @property
    def discount(self):
        return self.__discount

    def __init__(self, commodity, qty, discount):
        Purchase.__init__(self, commodity, qty)
        self.__discount = discount

    def __str__(self):
        return "{0};{1}".format(Purchase.__str__(self), self.discount)

    def get_cost(self):
        return (self.commodity.price - self.discount) * self.qty


class BulkPurchase(Purchase):

    @property
    def discount(self):
        return self.__discount

    LIMIT = 2

    def __init__(self, commodity, qty, discount):
        Purchase.__init__(self, commodity, qty)
        self.__discount = discount

    def __str__(self):
        return "{0};{1}".format(Purchase.__str__(self), self.discount)

    def get_cost(self):
        return self.commodity.price * self.qty * ((1-self.discount/100) if self.qty >= BulkPurchase.LIMIT else 1)