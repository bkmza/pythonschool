import sys
import csv
from pricepurchase import Purchase
from pricepurchase import PricePurchase
from bulkpurchase import BulkPurchase
from purchaseexception import CommodityException


_PURCHASES_CLASSES = {'Purchase':Purchase,
                      'PricePurchase':PricePurchase,
                      'BulkPurchase':BulkPurchase}

def get_data(file_name):
  with open(file_name, 'r') as input_file:
    return [get_class_from_factory(item) for item in csv.reader(input_file, delimiter=';') if len(item)]


def get_class_from_factory(rec):
  try:
    cls_obj = _PURCHASES_CLASSES[rec[0]]
    return cls_obj(*rec[1:])
  except CommodityException as cx:
    print '%s >> Error at line %d. Message: %s' % (rec, cx.line, cx.message)
  except KeyError:
    ce = CommodityException(0, 'Unknown class name')
    print '%s >> Error at line %d. Message: %s' % (rec, ce.line, ce.message)


#TODO
# Refactoring this method
# 1. encapsulate row printing and override methods
# 2. use array with length of columns instead variables
# 3. remove all print function. Use only one at the end of function
def display_diagnostic(data):
  CLASS_LEN = 15
  PRICE_LEN = 12
  COST_LEN = 12
  COMMODITY_LEN = len('Commodity')
  NUMBER_LEN = len('Number')
  DISCOUNT_LEN = len('Discount')
  columns = [
        ('Class name', CLASS_LEN, '<'),
        ('Commodity', COMMODITY_LEN, '<'),
        ('Price', PRICE_LEN, '>'),
        ('Number', NUMBER_LEN, '>'),
        ('Discount', DISCOUNT_LEN, '>'),
        ('Cost', COST_LEN, '>')
    ]
  print
  print 'Purchases list'.center(67)
  table_sep_line = '-' * 67
  print table_sep_line
  table_header = '|'.join('{0:^{1}}'.format(column[0], column[1]) for column in columns)
  print table_header
  print table_sep_line
  total_cost = 0
  for item in data:
    total_cost += item.get_cost()
    row = [(item.__class__.__name__, CLASS_LEN, '<'),
           (item.commodity.name, COMMODITY_LEN, '<'),
           (item.commodity.price, PRICE_LEN, '>'),
           (item.quantity, NUMBER_LEN, '>'),
           (item.discount if hasattr(item, 'discount') else '', DISCOUNT_LEN, '>'),
           (item.get_cost(), COST_LEN, '>')]
    print '|'.join('{0:{1}{2}}'.format(item[0], item[2], item[1]) for item in row)
  print table_sep_line
  last_row = [('Total',CLASS_LEN,'<'),
              ('', COMMODITY_LEN, '<'),
              ('', PRICE_LEN, '>'),
              ('', NUMBER_LEN, '>'),
              ('', DISCOUNT_LEN, '>'),
              (total_cost, COST_LEN, '>')]
  print ' '.join('{0:{1}{2}}'.format(item[0], item[2], item[1]) for item in last_row)

def main():
  try:
    data = get_data('in_task2_fifth.csv')
    items = [item for item in data if item is not None]
    if items:
      display_diagnostic(items)
  except IOError:
    print 'No input file'

if __name__ == '__main__':
  main()
  
