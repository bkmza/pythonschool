import csv
import os
import sys
import sqlite3
from utils import *
from testresultint import *
from testresultexception import TestResultException
from repgencsv import *





def main(folder_path, db_path):

  # part 1. Integer values and generete reports in csv
  print 'part 1. Integer values'
  rgc = ReportGeneratorCsv(db_path)
  tests = rgc.get_data(folder_path)
  rgc.save_to_db(tests)
  mean_marks = rgc.get_mean_marks()
  month_report = rgc.get_month_report()
  rgc.print_data('reports.csv', mean_marks, month_report)
  print

  # part 2. Float values (divided by 0.5) and generate reports in console
  print 'part 2. Float values'
  rgf = ReportGeneratorCsv('testsfloat.db')
  tests = rgf.get_data(folder_path)
  rgf.save_to_db(tests)


if __name__ == '__main__':
  if len(sys.argv) != 3:
    print 'usage: ./main.py {--folderpath} {--dbpath}'
    sys.exit(1)

  folder_path, db_path = sys.argv[1:]
  main(folder_path, db_path)
  
