from decorators import repr


@repr
class Liquid():

    @property
    def weight(self):
        return self.__weight

    @property
    def volume(self):
        return self.__volume

    def __init__(self, volume=1, density=1000):
        self.__volume = volume
        self.__weight = volume * density