import math
from containers.Container import Container

class Tube(Container):

    @property
    def volume(self):
        return self.__volume

    def check(self, content):
        cls_name = content.__class__.__name__
        if cls_name == "Liquid":
            return content.volume <= self.volume
        elif cls_name == "Tube":
            d_content, depth_content = content.out_metrics
            d, depth = self.out_metrics
            return d_content <= d and depth_content <= depth
        elif cls_name in ["Solid", "Box"]:
            width_content, height_content, depth_content = content.out_metrics
            d, depth = self.out_metrics
            return math.hypot(width_content, height_content) <= d
        return False


    @staticmethod
    def calculate_volume(d, depth):
        return math.pi * d**2 * depth / 4

    def __init__(self, content=None, density=2000, thickness=0.01, d=1, depth=1):
        if content:
            cls_name = content.__class__.__name__
            if cls_name == "Tube":
                d, depth = content.out_metrics
            elif cls_name in ["Box", "Solid"]:
                d, _, depth = content.out_metrics
            else:
                depth = content.volume*4 / (math.pi*d**2)
        in_metrics = [d, depth]
        out_metrics = [x+2*thickness for x in in_metrics]
        volume_in = Tube.calculate_volume(in_metrics[0], in_metrics[1])
        volume_out = Tube.calculate_volume(out_metrics[0], out_metrics[1])
        weight = (volume_out - volume_in) * density
        Container.__init__(self, weight, content, in_metrics, out_metrics)
        self.__volume = volume_in

