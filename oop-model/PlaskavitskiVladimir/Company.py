from containers.Box import Box
from containers.Tube import Tube

class Company():

    @staticmethod
    def sorting(items, rule={"Person": 0, "Box": 1, "Amorphous": 2, "Tube": 3}):
        return sorted(items, key=lambda item: rule[item.__class__.__name__])

    @staticmethod
    def wrapper(item):
        ctype = item.__class__.__name__
        if ctype is "Solid":
            pack = Box(item)
        elif ctype is "Liquid":
            pack = Tube(item)
        else:
            pack = item
        return pack

    @staticmethod
    def packing(items):
        packs = []
        for item in items:
            packs.append(Company.wrapper(item))
        return packs

    @staticmethod
    def measuring(items):
        return sum(item.weight for item in items)

    @staticmethod
    def checking_weight(items, parom):
        return Company.measuring(items) <= parom.capacity