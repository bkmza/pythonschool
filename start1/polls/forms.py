from django import forms
from polls.models import Task


class TaskForm(forms.ModelForm):
  type = forms.CharField(widget=forms.HiddenInput())
  date = forms.DateField(widget=forms.DateInput(format = '%d.%m.%Y'), input_formats=('%d.%m.%Y',))
  class Meta:
    model = Task
    labels = {
            'title': 'Title',
            'date': 'Date',
        }
    help_texts = {
            'title': '',
            'date': ''
        }
    widgets = { }
    exclude = ['is_fixed', 'user']


