from utils import test
from purchase import Purchase
  
class PricePurchase(Purchase):
  
  @property
  def discount(self):
    return self.__discount
  
  def __init__(self, name, price, count=1, discount=.0):
    Purchase.__init__(self, name, price, count)
    self.__discount = int(discount)
    
  def get_cost(self):
    return self.commodity.price * self.quantity - self.discount * self.quantity
  
  def __str__(self):
    return Purchase.__str__(self) + ';%s;%s' % (self.discount, self.get_cost())
    
    
if __name__ == '__main__':
  pp1 = PricePurchase('Beer', price=300, count=2, discount=50)
  pp2 = PricePurchase('Beer', price=300, count=2, discount=50)
  print pp1.__class__.__name__
  test(pp1.__str__(), 'Beer;300;2;50;500')
  test(pp2.__str__(), 'Beer;300;2;50;500')
  test(pp1.get_cost(), 500)
  test(pp2.get_cost(), 500)
  test(pp1 == pp2, True)
  test(pp2.__eq__(pp1), True)
   