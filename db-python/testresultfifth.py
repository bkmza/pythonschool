from testresultbase import TestResultBase
from testresultexception import TestResultException

class TestResultFifth(TestResultBase):

  def __init__(self, *args):

    login, status, date, mark = args
    TestResultBase.login = self.parse_login(login)
    TestResultBase.status = self.parse_status(status)
    TestResultBase.date = self.parse_date(date)
    TestResultBase.mark = self.parse_mark(mark)

  # task 2. integer and .5 digits. Range 0 - 20.
  def parse_mark(self, mark):

    try:
      mark = float(mark)
    except ValueError:
      raise TestResultException('Mark cannot parsed to float')

    if mark % 0.5:
      raise TestResultException('Mark should be integer or divided on 0.5')

    if mark > 20 or mark < 0:
      raise TestResultException('Mark cannot be less zero or greater 20')

    return mark