def repr(aClass):
    def spec_repr(self):
        return "{0}".format(self.__class__.__name__)
        #return "{0} ({1:0.2f})".format(self.__class__.__name__, self.weight)
    aClass.__repr__ = spec_repr
    return aClass