import sys
import math
import re


def get_segment_len(line, re_coords=re.compile(r'[\s();]+')):
    coords = re_coords.split(line)[1:-1]
    diff_x = float(coords[2]) - float(coords[0])
    diff_y = float(coords[3]) - float(coords[1])
    return  int(math.hypot(diff_x, diff_y) + 0.5)


def get_sequence(filename, sequence_factory, f_append):
    sequence = sequence_factory()    
    with open(filename, 'rU') as f:
        for line in f:
            f_append(sequence, get_segment_len(line))
    return sequence
  

def append_to_list(sequence, segment_len):
    if segment_len not in sequence:
        sequence.append(segment_len)


def append_to_set(sequence, segment_len):
    sequence.add(segment_len)


def main(sequence_factory, f_append):
    sequence = get_sequence(sys.argv[1], sequence_factory, f_append)
    print sorted(sequence)


if __name__ == '__main__':
    sequence_attrs = {
        '--l' : (list, append_to_list),
        '--s' : (set, append_to_set)
    }
    try:
        if sys.argv[2] in sequence_attrs: 
            sequence_factory, f_append = sequence_attrs[sys.argv[2]] 
            main(sequence_factory, f_append)
        else:
            print 'wrong second argument: --l or --s needed'
    except IOError:
        print 'wrong filename'
