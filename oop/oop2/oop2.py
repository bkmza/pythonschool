from pricepurchase import PricePurchase
from bulkpurchase import BulkPurchase
import csv

# TODO
# 1. Hash code calc next: return hash((self.name, self.price))
# 2. Use ABCMeta for abstract class and method
# 3. Use dict() + filter() builtin than FOREACH or IF to sort and filter
# 4. Check arrays after filter and remove empty

PRICE_PURCHASE = 'PricePurchase'
BULK_PURCHASE = 'BulkPurchase'


def get_data(file_name):
  with open(file_name, 'r') as input_file:
    return [get_class_from_factory(item) for item in csv.reader(input_file, delimiter=';')]


def get_class_from_factory(rec):
  if rec[0] == PRICE_PURCHASE: return PricePurchase(*rec[1:])
  if rec[0] == BULK_PURCHASE: return BulkPurchase(*rec[1:])


def get_commodities(data):
  dict = {}
  for item in data:
    if item.commodity not in dict:
      dict[item.commodity] = []
    dict[item.commodity].append(item)
  return dict


def main():
  data = get_data('in_task2_fifth.csv')
  cmds = get_commodities(data)

  # Remove records with price less 10000
  cmds = dict((k, v) for (k, v) in cmds.iteritems() if k.price > 10000)

  # Remove purchases with cost less 50000
  for cmd in cmds:
    cmds[cmd] = [item for item in cmds[cmd] if item.get_cost() > 50000]

  # Sort by non-decreasing number of purchases
  # after by non-increasing the total cost of purchases in the list
  cmds = sorted(cmds.items(), key=lambda x: (len(x[1]), -sum(purchase.get_cost() for purchase in x[1])))

  # Print results
  for cmd in cmds:
      print cmd

if __name__ == '__main__':
  main()
  
