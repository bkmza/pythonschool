from businessobjects import Purchase, PricePurchase, BulkPurchase
import csv

# TODO:
# 1 Remove try-except with return None. ++
# 2 main.get_data without generators. ++
# 3 dictionary, cls_name and value - class --
# 4 return purchase_factory[rec[0]](*rec[1:]) - read data from file --
# 5 Comparison of objects should performed via the operator '==', not __eq__(). ++
# 6 Remove print operation from get_statistic method. --

# Second review:
# 1. Check instance of class: return isinstance(that, Purchase) and self.name
# 2. Better:   def __str__(self): return '%s;%s’ % (Purchase.__str__(self),self.discount)
# 3. Add cost to row like last item: return ';'.join([str(x) for x in self._fields_values() + [self.get_cost()]])
## def _fields_values(self):
##    class_name = self.__class__.__name__
##    return [class_name, self._name, self._price, self._count]
### and override that method in sub-classes
### def _fields_values(self):
###    return super(PricePurchase, self)._fields_values() + [self._price_discount]
# 4. Add check to not-exist file.

PURCHASE = 'Purchase'
PRICE_PURCHASE = 'PricePurchase'
BULK_PURCHASE = 'BulkPurchase'

def get_data(file_name):
  with open(file_name, 'r') as input_file:
    return [get_class_from_factory(item) for item in csv.reader(input_file, delimiter=';')]

def get_class_from_factory(rec):
  c_name, p_name, price, count = rec[0], rec[1], int(rec[2]), int(rec[3])
  if c_name == PURCHASE: return Purchase(p_name, price, count)
  else:
    discount = float(rec[4])
    if c_name == PRICE_PURCHASE: return PricePurchase(p_name, price, count, discount)
    if c_name == BULK_PURCHASE: return BulkPurchase(p_name, price, count, discount)
  
def print_statistic(first, rest):
  is_equals = True
  max_cost = first.get_cost()
  print first
  while len(rest):
    print rest[0]
    i_cost = rest[0].get_cost()
    if(max_cost < i_cost):
      max_cost = i_cost
    curr = rest.pop(0)
    if not first == curr:        
      is_equals = False
    first = curr
  print 'Max cost: %s. All objects are equal: %s' % (max_cost, is_equals)
  
def main():
  items = get_data('in_task2_fifth.csv')
  if items:
    print_statistic(items[0], items[1:])


if __name__ == '__main__':
  main()
  
