# module with reusable test utilities for google pyclass style testing
import math

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))

def expect(expected_error_class, test_code):
  try:
    test_code()
    test(None, expected_error_class)
  except Exception as e:
    test(e.__class__, expected_error_class)


def get_median(values):
  try:
    mid_num = ( len( values ) - 1) / 2
    return values[ mid_num ]
  except TypeError:
    ceil = int( math.ceil( mid_num ) )
    floor = int( math.floor( mid_num ) )
    return ( values[ ceil ] + values[ floor ] ) / 2


def get_lower_quartile(values):
  try:
    low_mid = ( len( values ) - 1 ) / 4
    return values[ low_mid ]
  except TypeError:
    ceil = int( math.ceil( low_mid ) )
    floor = int( math.floor( low_mid ) )
    return ( values[ ceil ] + values[ floor ] ) / 2


def get_upper_quartile(values):
  try:
    high_mid = ( len( values ) - 1 ) * 0.75
    return values[ high_mid ]
  except TypeError:
    ceil = int( math.ceil( high_mid ) )
    floor = int( math.floor( high_mid ) )
    return ( values[ ceil ] + values[ floor ] ) / 2
