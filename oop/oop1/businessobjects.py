from utils import test

# TODO:
# 1. Use private fields. Explain @property attribute. ++
# 2. Price field - only integer type. Remove typecast to int. ++
# 3. Get cost from super class. ++
# 4. COUNT_TO_DISCOUNT field pick up to all instances. ++
# 5. Add get_cost to _str_ method. ++
# 6. def __init__(self, *args) --

class Purchase(object):
  
  @property
  def name(self):
    return self.__name
  
  @property
  def price(self):
    return self.__price
  
  @property
  def count(self):
    return self.__count
  
  def __init__(self, name, price, count=1):
    self.__name = name
    self.__price = price
    self.__count = count
  
  def get_cost(self):
    return self.price * self.count
  
  def __str__(self):
    props = [self.name, self.price, self.count, self.get_cost()]
    return ';'.join(map(lambda x: str(x), props))
  
  def __eq__(self, that):
    return self.name == that.name and self.price == that.price
  
  
class PricePurchase(Purchase):
  
  @property
  def discount(self):
    return self.__discount
  
  def __init__(self, name, price, count=1, discount=0):
    Purchase.__init__(self, name, price, count)
    self.__discount = discount
    
  def get_cost(self):
    return Purchase.get_cost(self) - self.discount * self.count
  
  def __str__(self):
    return Purchase.__str__(self) + ';%s' % str(self.discount)
    

class BulkPurchase(Purchase):
  
  __DISCOUNT_CONDITION = 15
  
  @property
  def discount(self):
    return self.__discount
  
  def __init__(self, name, price, count=1, discount=0):
    Purchase.__init__(self, name, price, count)
    self.__discount = discount

  def get_cost(self):
    cost = super(BulkPurchase, self).get_cost()
    if self.count > BulkPurchase.__DISCOUNT_CONDITION:
      cost *= (1 - self.discount/100)
    return int(cost + 0.5)

  def __str__(self):
    return Purchase.__str__(self) + ';%s' % str(self.discount)


if __name__ == '__main__':
  # Purchase
  p1 = Purchase('Snikers', price=5, count=10)
  p2 = Purchase('Mars', price=4, count=5)
  print p1.__class__.__name__
  test(p1.__str__(), 'Snikers;5;10;50')
  test(p2.__str__(), 'Mars;4;5;20')
  test(p1.get_cost(), 50)
  test(p2.get_cost(), 20)
  test(p1.__eq__(p2), False)
  test(p2 == p1, False)
  
   # PricePurchase
  print
  pp1 = PricePurchase('Beer', price=300, count=2, discount=50)
  pp2 = PricePurchase('Beer', price=300, count=2, discount=50)
  print pp1.__class__.__name__
  test(pp1.__str__(), 'Beer;300;2;500;50')
  test(pp2.__str__(), 'Beer;300;2;500;50')
  test(pp1.get_cost(), 500)
  test(pp2.get_cost(), 500)
  test(pp1 == pp2, True)
  test(pp2.__eq__(pp1), True)
   
  # BulkPurchase
  print
  bp1 = BulkPurchase('Christmas tree', price=500, count=20, discount=5.825)
  bp2 = BulkPurchase('Deadtree book', 500, 10, 17.5)
  print bp1.__class__.__name__
  test(bp1.__str__(), 'Christmas tree;500;20;9418;5.825')
  test(bp2.__str__(), 'Deadtree book;500;10;5000;17.5')
  test(bp1.get_cost(), 9418)
  test(bp2.get_cost(), 5000)
  test(bp1.__eq__(bp2), False)
  test(bp2.__eq__(bp1), False)