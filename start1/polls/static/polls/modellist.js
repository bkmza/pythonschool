var List = function (settings) {
    var ex = {
        },
        doInitPage = function() {
          doBindCheckboxes();
          doBindRemoveBtn();
          doBindFixBtn();
        },
        doBindCheckboxes = function(){
          console.log('--> doBindCheckboxes bind')
          $(':checkbox').change(function(){
            if(this.checked){
            }
            else{
            }
          })
        },
        doBindRemoveBtn = function(){
          console.log('--> doBindRemoveBtn bind')

          var token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
          $('.btn-remove').on('click', function(){
            var ids = []
            $.each($(':checked'), function( index, item ) {
              ids.push($(item).attr('data-id'))
            });
            utils.doAjax('/polls/remove', {csrfmiddlewaretoken: token, ids: ids}, doCallbackRemove);
          })
        },
        doBindFixBtn = function() {
          console.log('--> doBindFixBtn bind')

          var token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
          $('.btn-fix').on('click', function(){
            var ids = []
            $.each($(':checked'), function( index, item ) {
              ids.push($(item).attr('data-id'))
            });
            utils.doAjax('/polls/fix', {csrfmiddlewaretoken: token, ids: ids}, doCallbackFix);
          })
        },
        doCallbackRemove = function(data){
          if(data && data.is_success){
            $.each($(':checked'), function( index, item ) {
              $(item).closest('tr').remove();
            })
            if ($(':checked').length == 0) { location.reload(); }
          }
        },
        doCallbackFix = function(data){
          if(data && data.is_success){
            $.each($(':checked'), function( index, item ) {
              $(item).closest('tr').remove();
            })
            if ($(':checked').length == 0) { location.reload(); }
          }
        }



    return {
        initPage: doInitPage
    }
}