from operator import mul
from containers.Container import Container

class Box(Container):

    def check(self, content):
        cls_name = content.__class__.__name__
        if cls_name == "Tube":
            width, height, depth = content.out_metrics
            d_content, depth_content = self.out_metrics
            return d_content <= width and d_content <= height and depth_content <= depth
        elif cls_name in ["Solid", "Box"]:
            print content.out_metrics
            for a, b in zip(sorted(self.in_metrics), sorted(content.out_metrics)):
                if a < b:
                    return False
            return True
        return content is None


    def __init__(self, content=None, density=2000, thickness=0.01, width=1, height=1, depth=1):
        if content:
            cls_name = content.__class__.__name__
            if cls_name == "Tube":
                width, depth = content.out_metrics
                height = width
            else:
                width, height, depth = content.out_metrics
        in_metrics = [width, height, depth]
        out_metrics = [x+2*thickness for x in in_metrics]
        weight = (reduce(mul, out_metrics) - reduce(mul, in_metrics)) * density
        Container.__init__(self, weight, content, in_metrics, out_metrics)
