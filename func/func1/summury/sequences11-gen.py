import sys
import math
import re


def get_segment_len(line, re_coords=re.compile(r'[\s();]+')):
    coords = re_coords.split(line)[1:-1]
# initial and last elements are discarded    
    diff_x = float(coords[2]) - float(coords[0])
    diff_y = float(coords[3]) - float(coords[1])
    return  int(math.hypot(diff_x, diff_y) + 0.5)


def get_lines(filename):
    with open(filename, 'r') as f:
        for line in f:
            yield line


def append_unique_lengths(unique_lengths, item):
    if item not in unique_lengths:
       unique_lengths.append(item)
    return unique_lengths

def init_list(func):
    def wrapper(item):
        return func(wrapper.unique_list, item)
    wrapper.unique_list = []
    return wrapper

@init_list
def is_appended(unique_lengths, item):
    flag = item not in unique_lengths
    if flag:
      unique_lengths.append(item)
    return flag


# def main(filename):
#     try:
#          lines = get_lines(filename)
#          f_segment_lengths = (get_segment_len(line) for line in lines)
#          unique_lengths = []
#          f_unique_lengths = (x for x in f_segment_lengths if is_appended(unique_lengths, x))
#          print sorted(f_unique_lengths)
#     except IOError:
#         print 'wrong filename'
        
def main(filename):
    try:
      print sorted((x for x in (get_segment_len(line) for line in get_lines(filename)) if is_appended(x)))
    except IOError:
        print 'wrong filename'


if __name__ == '__main__':
    main('in.txt')


