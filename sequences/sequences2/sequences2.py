import sys
import re
import math

REGEX_FLOAT = r'([eE\d.-]+)'
REGEX_POINT = r'\(\s*%s\s*\;\s*%s\s*\)' % (REGEX_FLOAT, REGEX_FLOAT)
REGEX_LINE = r'\s*%s\s*%s\s*' % (REGEX_POINT, REGEX_POINT)

def process(file_in):
  try:
      input_file = open(file_in, "r")
  except:
      return 'Input file not found'
    
  sequence = {}
  for line in input_file:
    for x1, y1, x2, y2 in re.findall(REGEX_LINE, line):
      len = get_line_length(x1,y1,x2,y2)
      rlen = int(round(len))
      append_to_sequence(sequence, rlen)
  
  input_file.close()
  return sorted(sequence.items(), key=lambda(k,v):(v,k), reverse=True)
  

def append_to_sequence(sequence, len):
  if len in sequence:
    sequence[len] += 1
  else:
    sequence[len] = 1

def get_line_length(x1,y1,x2,y2):
  return math.sqrt((float(x1) - float(x2)) ** 2 + ((float(y1) - float(y2)) ** 2))

      
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'sequences2'
  test(process('not_exists_file.txt'), 'Input file not found')
  test(process('in.txt'), [(3, 5), (10, 3), (1, 1)])

if __name__ == '__main__':
  main()

