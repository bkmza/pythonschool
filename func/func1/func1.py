import re
import sys
import math
import bisect

def process(file_name):
  try:
    get_line = iter(read_line(file_name))
  except TypeError as e:
    return "TypeError ({%s})" % (e)
  except IOError as e:
    return "I/O error({%s}): {%s}" % (e.errno, e.strerror)

  seq = []
  for line in get_line:
    coords = get_coords(line)
    line_len = get_line_len(*coords)
    push(seq, line_len)
  return seq

def push(seq, len):
    if not contains(seq, len):
      bisect.insort_left(seq, len)

def contains(list, value):
    index = bisect.bisect_left(list, value)
    if index < len(list):
        return list[index] == value
    return False
  
def get_line_len(*coords):
  X1 = 0
  X2 = 2
  Y1 = 1
  Y2 = 3
  diff_x = float(coords[X2]) - float(coords[X1])
  diff_y = float(coords[Y2]) - float(coords[Y1])
  return  int(math.hypot(diff_x, diff_y) + 0.5)

def get_coords(line, re_coords=re.compile(r'[\s();]+')):
    return (val for val in re_coords.split(line) if len(val) > 0)

def read_line(file_name):
  # File not closed
  return (line.rstrip() for line in open(file_name, "rU"))

def read_line_yield(file_name):
  # File closed
  file_object = open(file_name, "rU")
  for line in file_object:
    yield line
  file_object.close()
      
      
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'sequences1'
  test(process('not_exists_file.txt'), 'I/O error({2}): {No such file or directory}')
  test(process('in.txt'), [1, 3])

if __name__ == '__main__':
  main()

