from utils import test

class Commodity(object):
  
  @property
  def name(self):
    return self.__name
  
  @property
  def price(self):
    return self.__price

  def __init__(self, name, price):
    self.__name = name
    self.__price = int(price)
  
  def __str__(self):
    props = [self.name, self.price]
    return ';'.join(map(lambda x: str(x), props))
 
  def __eq__(self, that):
    return hasattr(that, 'name') and self.name == that.name and hasattr(that, 'price') and self.price == that.price
  
  def __hash__(self):
    return hash(self.name) ^ hash(self.price)
  
  def __getitem__(self, key):
    val = dict.__getitem__(self, key)
    return val
  
  def __repr__(self):
    return "%s;%s;%s" % (self.__class__.__name__, self.name, self.price)
  
if __name__ == '__main__':
  c1 = Commodity('Snikers', price=5)
  c2 = Commodity('Mars', price=4)
  print c1.__class__.__name__
  test(c1.__str__(), 'Snikers;5')
  test(c2.__str__(), 'Mars;4')
