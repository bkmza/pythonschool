import sys
import math
import re


def get_segment_len(line, re_coords=re.compile(r'[\s();]+')):
    coords = re_coords.split(line)[1:-1]
# initial and last elements are discarded    
    diff_x = float(coords[2]) - float(coords[0])
    diff_y = float(coords[3]) - float(coords[1])
    return  int(math.hypot(diff_x, diff_y) + 0.5)


def get_sequence(filename):
    sequence = []
    with open(filename, 'rU') as f:
        for line in f:
            append_to_list(sequence, get_segment_len(line))
    return sequence

  
def append_to_list(sequence, segment_len):
    if segment_len not in sequence:
        sequence.append(segment_len)


def main(filename):
    try:
        sequence = get_sequence(filename)
        print sorted(sequence)
    except IOError:
        print 'wrong filename'


if __name__ == '__main__':
    main(sys.argv[1])
