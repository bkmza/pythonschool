# module with reusable test utilities for google pyclass style testing

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))

def expect(expected_error_class, test_code):
  try:
    test_code()
    test(None, expected_error_class)
  except Exception as e:
    test(e.__class__, expected_error_class)
