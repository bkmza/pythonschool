__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from Cargo import Cargo


class Platform(Cargo):

    def __init__(
            self,
            name="Platform",
            cargo_weight=30*1000
    ):
        self._cargo_weight = cargo_weight
        super(Platform, self).__init__(name)

    def _calculate_weight(self):
        return self._cargo_weight