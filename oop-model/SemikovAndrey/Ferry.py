__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from Cargo import Cargo
from Passenger import Passenger
from texttable import Texttable


class Ferry(object):
    MAX_WEIGHT = 1000 * 1000  # 1000 tons
    MAX_PASSENGERS = 200

    def __init__(self):
        self._cargoes = []
        self._total_weight = 0
        self._total_passengers = 0

    def add_passenger(self, passenger):
        """
        add passenger and give him a place number
        """
        if not isinstance(passenger, Passenger):
            raise TypeError("object of type %s should be given" % Passenger.__name__)
        passenger.set_seat_number(self._total_passengers + 1)
        self._cargoes.append(passenger)
        self._total_passengers += 1

    def add_cargo(self, cargo):
        """
        add cargo
        """
        if not isinstance(cargo, Cargo):
            raise TypeError("object of type %s should be given" % Cargo.__name__)
        self._cargoes.append(cargo)
        self._total_weight += cargo.weight

    @property
    def cargoes(self):
        return self._cargoes

    @property
    def total_passengers(self):
        return self._total_passengers

    @property
    def total_weight(self):
        return self._total_weight


    def recount_total_weight(self):
        self._total_weight = 0
        for cargo in self._cargoes:
            if not isinstance(cargo, Cargo):
                continue
            self._total_weight += cargo.weight

    def are_passengers_and_cargo_fit(self):
        """
        determine if passengers and cargo weight fit into the ferry
        """
        return \
            self.MAX_PASSENGERS >= self._total_passengers and \
            self.MAX_WEIGHT > self._total_weight

    def sort(self, key=None):
        """
        sort cargoes with given order
        """
        self._cargoes.sort(key=key)

    def __str__(self):
        """
        Draw a table of cargoes and add total
        """
        data_table = Texttable()
        data_table.set_deco(Texttable.BORDER | Texttable.HEADER | Texttable.VLINES)
        data_table.set_cols_dtype(['t',
                                  'i',
                                  'f'])
        data_table.set_cols_align(["l", "l", "l"])
        data_table.header(["Name", "Seat number", "Cargo weight (tons)"])
        for cargo in self.cargoes:
            if isinstance(cargo, Cargo):
                data_table.add_row([cargo.name, "", round(cargo.weight / 1000, 2)])
                continue
            elif isinstance(cargo, Passenger):
                data_table.add_row([cargo.name, cargo.seat_number, ""])
                continue
            raise TypeError("Cargo should be of type % or %s" % (Cargo.__name__, Passenger.__name__))

        total_table = Texttable()
        total_table.set_deco(Texttable.HEADER)
        total_table.set_cols_dtype(['t',
                                   'i',
                                   'f'])
        total_table.header(
            [
                "Total:",
                "Passengers: %d" % self._total_passengers,
                "Weight: %f tons" % round(self._total_weight / 1000, 2)
            ]
        )

        return data_table.draw() + "\n" + total_table.draw()

    def __repr__(self):
        return self.__str__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self._cargoes

    def __enter__(self):
        return self