from __future__ import print_function
from inspect import getmro
from itertools import groupby
from operator import methodcaller


class Ship(object):
    def __init__(self, name, deadweight_tonnage, max_passengers, items=None):
        self._deadweight_tonnage = deadweight_tonnage
        self._name = name
        self._max_passengers = max_passengers
        if items is None:
            self._items = []
        else:
            self._items = items

    def __repr__(self):
        return '[\'{}\', {}t/{}t{}]'.format(
            self._name, self.total_weight,
            self._deadweight_tonnage,
            ', OVERWEIGHTED' if self.is_overweighted else '')

    def add_item(self, item):
        self._items.append(item)

    def get_items(self):
        return self._items

    @property
    def max_passengers(self):
        return self._max_passengers

    @property
    def total_weight(self):
        total_weight = 0
        for item in self._items:
            total_weight += item.weight
        return total_weight

    @property
    def is_overweighted(self):
        if self.total_weight > self._deadweight_tonnage:
            return True
        else:
            return False

    def sort_items_by_kind(self, sort_classnames):
        '''
        Sort items in order of first appearance of the item's class name
        in given list of class names. Move items to the end of result list
        if their class names not presented in given list.
        '''
        kind_dict = {}
        kind_dict[None] = []
        for kind in sort_classnames:
            kind_dict[kind] = []

        get_kind = methodcaller('within_classes', sort_classnames)
        for kind, group in groupby(self._items, get_kind):
            kind_dict[kind].extend(group)

        ordered_items = []
        added_kinds = set()
        for kind in sort_classnames:
            if kind not in added_kinds:
                ordered_items.extend(kind_dict.get(kind, []))
                added_kinds.add(kind)
        ordered_items.extend(kind_dict[None])

        self._items = ordered_items

    def count_items_by_classname(self):
        count_dict = {}
        for item in self._items:
            item_classname = item.__class__.__name__
            count_dict[item_classname] = count_dict.get(item_classname, 0) + 1
        return count_dict


class Weightable(object):
    def __init__(self, weight):
        self._weight = weight

    @property
    def weight(self):
        return self._weight

    @classmethod
    def within_classes(cls, class_names_list):
        for class_name in class_names_list:
            for c in getmro(cls):
                if c.__name__ == class_name:
                    return class_name

    def __repr__(self):
        return '<{}, {}t>'.format(self.__class__.__name__, self.weight)


class Cargo(Weightable):
    pass


class FillableCargo(Cargo):
    def __init__(self, empty_weight, volume, content_weight):
        self._empty_weight = empty_weight
        self._volume = volume
        Weightable.__init__(self, empty_weight + content_weight)

    @property
    def empty_weight(self):
        return self._empty_weight

    @property
    def volume(self):
        return self._volume

    def set_weight_by_uniform_density(self, content_density, filling=1.0):
        Weightable.__init__(self, self.empty_weight +
                            content_density * self.volume * filling)

    @classmethod
    def init_by_dencity(cls, content_density, filling=1.0):
        fillable_object = cls()
        fillable_object.set_weight_by_uniform_density(content_density, filling)
        return fillable_object


class Container(FillableCargo):
    pass


class TwentyFeetContainer(Container):
    def __init__(self, content_weight=0):
        Container.__init__(self, 2.2, 33.1, content_weight)


class FourtyFeetContainer(Container):
    def __init__(self, content_weight=0):
        Container.__init__(self, 3.8, 67.5, content_weight)


class Platform(Cargo):
    pass


class Tank(FillableCargo):
    pass


class TwentyFeetTank(Tank):
    def __init__(self, content_weight=0):
        Tank.__init__(self, 7.8, 24, content_weight)


class FourtyFeetTank(Tank):
    def __init__(self, content_weight=0):
        Tank.__init__(self, 14.8, 51.5, content_weight)


class Passenger(Weightable):
    def __init__(self):
        Weightable.__init__(self, 0.1)


def main():
    cf = Ship('RMS St Helena', 1800, 5)
    cf.add_item(Platform(1799))
    cf.add_item(TwentyFeetContainer(10))
    cf.add_item(TwentyFeetContainer.init_by_dencity(1.1))
    cf.add_item(TwentyFeetTank.init_by_dencity(0.7, 0.5))
    cf.add_item(FourtyFeetContainer.init_by_dencity(2.1))
    cf.add_item(FourtyFeetTank.init_by_dencity(0.2))
    for i in range(6):
        cf.add_item(Passenger())
    print(cf.get_items())
    cf.sort_items_by_kind(['Passenger', 'Container', 'Platform', 'Tank'])
    print(cf.get_items())
    print(cf)
    if cf.count_items_by_classname()['Passenger'] > cf.max_passengers:
        print('Too much passengers on the board!')


if __name__ == '__main__':
    main()
