__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from Platform import Platform
from Ferry import Ferry
from Passenger import Passenger
from CylindricalTank import CylindricalTank
from SeaContainer import SeaContainer
from Cargo import Cargo


def sort_order(cargo):
    """
    configure sort order
    """
    if isinstance(cargo, Passenger):
        return 1
    elif isinstance(cargo, SeaContainer):
        return 2
    elif isinstance(cargo, Platform):
        return 3
    elif isinstance(cargo, CylindricalTank):
        return 4
    raise TypeError("% object or %s object of type should be given" % (Passenger.__name__, Cargo.__name__))


def main():
    with Ferry() as private_ferry:
        for index in range(1, 10):
            private_ferry.add_passenger(Passenger())
            private_ferry.add_cargo(CylindricalTank())
            private_ferry.add_cargo(SeaContainer())
            private_ferry.add_cargo(Platform())

        print private_ferry

        private_ferry.sort(key=sort_order)

        print private_ferry

        print "This amount of passengers (%d) and cargo (%f tons) %s into the ferry" \
              % \
              (
                    private_ferry.total_passengers,
                    round(private_ferry.total_weight / 1000, 2),
                    (private_ferry.are_passengers_and_cargo_fit() and ["fit"] or ["does not fit"])[0]
              )



# Standard boilerplate to call the main() function.
if __name__ == '__main__':
    main()
