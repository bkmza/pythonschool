import csv

MINUS = "-"
PLUS_WS = " + "
MINUS_WS = " - "
MINUS_LENGTH = len(MINUS)
PLUS_WS_LENGTH = len(PLUS_WS)
ZERO = "0"
ZERO_LENGTH = len(ZERO)
MAX_REPLACE_COUNT = 1
START_INDEX = 0

def process(file_name):
    try:
        input_file = open(file_name, "rb")
    except:
        return 'Input file not found'

    calc = ''
    result_value = 0
    errors = 0

    reader = csv.reader(input_file, delimiter=';')
    for row in reader:
        try:
            if len(row) > START_INDEX and is_int(row[START_INDEX]):
                index = int(row[START_INDEX])
            else:
                errors += 1
                continue
            
            if len(row) > index and is_float(row[index]):
                fval = float(row[index])    
            else:
                errors += 1
                continue
            
            #calc += PLUS_WS + str(fval) if fval >= 0 else MINUS_WS + str(fval)[MINUS_LENGTH:]
            # Without operations with constants
            calc += PLUS_WS if fval >= 0 else MINUS_WS
            calc += str(abs(fval))
            result_value += fval
        except:
            errors += 1
            
    input_file.close()
    # calc = calc[PLUS_WS_LENGTH:] if calc.startswith(PLUS_WS) else calc.replace(MINUS_WS, MINUS, MAX_REPLACE_COUNT)
    # Solution from example
    if len(calc):
        MINUS_WS_LENGTH = len(MINUS_WS)
        SIGN_POS = MINUS_WS.index(MINUS)
        symbol = calc[SIGN_POS]
        calc = calc[MINUS_WS_LENGTH:]
        if symbol == MINUS:
            calc = '%s%s' % (MINUS, calc)
        
    return '''result(%s) = %s error-lines = %d''' % (
        calc,
        '0.0' if result_value == 0 else '{0:g}'.format(result_value),
        errors)

def is_int(sval):
    sval = sval.strip()
    if sval.startswith(MINUS):
        sval = sval.replace(MINUS, '', MAX_REPLACE_COUNT).strip()
    
    if sval.startswith(ZERO) and len(sval) > ZERO_LENGTH:
        return False

    for let in sval:
        if not let.isdigit():
            return False
    return True

def is_float(sval):
    try:
        float(sval)
    except ValueError:
        return False
    return True

                                             
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'strings1'
  test(process('test0.csv'), 'Input file not found')
  test(process('test1.csv'), '''result(5.2 - 3.14 + 0.0) = 2.06 error-lines = 3''')
  test(process('test2.csv'), '''result(-3.1 - 1.0) = -4.1 error-lines = 0''')
  test(process('test3.csv'), '''result(0.75) = 0.75 error-lines = 0''')
  test(process('test4.csv'), '''result(0.0) = 0.0 error-lines = 0''')
  test(process('test5.csv'), '''result() = 0.0 error-lines = 1''')

if __name__ == '__main__':
  main()

