from abc import ABCMeta, abstractmethod
import csv
import os
import sys
import sqlite3
from utils import *
from testresultint import *
from testresultexception import TestResultException

class ReportGeneratorBase():
  __metaclass__ = ABCMeta

  @abstractmethod
  def print_data(self, *args):
    pass

  @abstractmethod
  def save_to_db(self, tests):
    pass

  @abstractmethod
  def get_mean_marks(self):
    pass

  @abstractmethod
  def get_month_report(self):
    pass

  @abstractmethod
  def get_data(self, folder_path):
    pass

