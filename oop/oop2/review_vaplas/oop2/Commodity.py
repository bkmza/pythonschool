class Commodity():
    
    @property
    def name(self):
        return self.__name
        
    @property
    def price(self):
        return self.__price    

    def __init__(self, name, price):
        self.__name = name
        self.__price = price

    def __eq__(self, other):
        return self.name == other.name and self.price == other.price   
    
    def __str__(self):
        return "{0};{1}".format(self.name, self.price)

    def __hash__(self):
        return hash((self.name, self.price))