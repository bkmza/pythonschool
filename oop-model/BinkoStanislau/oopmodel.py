#!/usr/bin/env python

from utils import test, expect
from collections import namedtuple
from math import pi
from pprint import pprint

# I suppose this solution is rather different from expected officially
# correct version, but still posting it since iterested to discuss pros/cons.
#
# Using namedtuple as base class everywhere because it provides
# a lot of useful "machinery" like __str__/properties/constructor/etc
# and allows to implement rather concise solution.
#
# One disadvantage of the approach is that ABCMeta seems to not do correct
# abstract methods/properties checking when combined with namedtuple, so replacing
# it with documentation (anyway I believe documentational part of ABC is the
# most important one - probably another one controversial statement)


class Boat(namedtuple('Boat', 'capacity items')):
  """Represents boat capable of lifting some items with
  given capacity limit. Elements of items collection are
  expected to have `weight` attribute.
  """
  def __new__(cls, capacity, items):
    # force using tuple to make class essentially immutable
    return super(cls, Boat).__new__(cls, capacity, tuple(items))

  def total_weight(self):
    return sum(c.weight for c in self.items)

  def can_lift(self):
    return self.total_weight() <= self.capacity


class Person(namedtuple('Person', 'name weight')):
  """Represents person with given name and weight attributes"""
  ORDER = 1


class BaseContainer(object):
  """Abstract base class for container of any shape with homogeneous content.
  Inheritors should define `volume`, `density` and `own_weight` attributes.
  """

  @property
  def weight(self):
    return self.volume * self.density + self.own_weight


class RectangularContainer(BaseContainer, namedtuple('RectangularContainer', 'density x y z own_weight')):
  """Container of rectangular shape"""
  ORDER = 2
  
  @property
  def volume(self):
    return self.x * self.y * self.z


class Platform(namedtuple('Platform', 'weight')):
  """Platform capable of holding anything, with zero own weight.
  Initialized just with weight of its content.
  """
  ORDER = 3


class CircularContainer(BaseContainer, namedtuple('CircularContainer', 'density radius height own_weight')):
  """Container of cylinder shape"""
  ORDER = 4

  @property
  def volume(self):
    return (self.radius ** 2) * pi * self.height


def sorted_items(items):
  """Sort given cargos/people according to class-level ORDER"""
  return sorted(items, key = lambda item: item.ORDER)


def run():
  """Runs scenario describen in assignment"""
  items = [
    RectangularContainer(density = 10, x = 2, y = 3, z = 4, own_weight = 5),
    CircularContainer(density = 11, radius = 12, height = 10, own_weight = 5),
    RectangularContainer(density = 5, x = 1, y = 2, z = 3, own_weight = 4),
    CircularContainer(density = 10, radius = 10, height = 5, own_weight = 7),
    Platform(weight = 500),
    Person(name = 'Some guy 1', weight = 80),
    Person(name = 'Some guy 2', weight = 75),
    Platform(weight = 200),
    ]
  
  print '\nInitial list:'
  pprint(items)

  items = sorted_items(items)

  print '\nSorted list:'
  pprint(items)

  weights = [item.weight for item in items]
  print '\nItem weights are %s total %s' % (weights, sum(weights))

  boat = Boat(capacity = 100500, items = items)
  print '\nCan lift for boat with capacity %s detected to be %s' % (boat.capacity, boat.can_lift())

  boat = Boat(capacity = 50100, items = items)
  print 'Can lift for boat with capacity %s detected to be %s' % (boat.capacity, boat.can_lift())


def main():
  print 'Running unit tests'
  test(Person(name = 'name', weight = 80).weight, 80)
  test(Platform(weight = 200).weight, 200)
  test(RectangularContainer(density = 1, x = 2, y = 3, z = 4, own_weight = 5).weight, 29)
  test(CircularContainer(density = 1, radius = 2, height = 1, own_weight = 1).weight, 1 + 4*pi)
  
  boat = Boat(capacity = 120, items = [
      Person(name = 'name', weight = 80),
      Platform(weight = 200),
      RectangularContainer(density = 1, x = 2, y = 3, z = 4, own_weight = 5),
      CircularContainer(density = 1, radius = 2, height = 1, own_weight = 1)])
  test(boat.total_weight(), 310 + 4*pi)
  test(boat.can_lift(), False)
  test(boat._replace(capacity = 500).can_lift(), True)
  
  print
  print 'Running test schenario'
  run()
  

if __name__ == '__main__':
  main()
