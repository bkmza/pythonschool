#!/usr/bin/env python

import csv
from itertools import imap, islice
from utils import test

#
# Comparing to previous version this solution tries to minimize count
# of intermediate lists created - only a single list will be used here.
# 
# It is possible to get rid of intermediate list, but this would lead
# to the worse design I believe, so leaving it implemented with one list.
#


def read_csv(filepath):
  """Returns csv rows from given file"""
  with open(filepath, 'rU') as csvfile:
    for row in csv.reader(csvfile, delimiter = ';'):
      yield row


def extract_row_value(row):
  """Extracts float value pointed to by parsed row[0] and 
     returns either value or None if row is invalid"""
  try:
    idx = int(row[0])
    if idx < 0 or idx >= len(row):
      raise IndexError()
    value = float(row[idx])
    return value
  except (ValueError, IndexError):
    return None


def filter_count_mismatch(predicate, iterable):
  """Similar to builtin filter function, but returns also
     count of non-matching elements"""
  matching_values = []
  non_matching_count = 0
  for value in iterable:
    if predicate(value):
      matching_values.append(value)
    else:
      non_matching_count += 1
  return (matching_values, non_matching_count)


def calculate_result(rows):
  """Extracts values from given rows sequence and returns tuple:
     (list of float values, sum of values, count of invalid rows)"""
  extract_results = imap(extract_row_value, rows) # not creating list on this line
  values, errors_count = filter_count_mismatch(lambda v: v is not None, extract_results)
  total = sum(values, 0.0) # in case of empty list need float zero, not int
  return (values, total, errors_count)


def format_result((values, total, errors_count)):
  """Accepts csv calculation result as tuple (in format returned by calculate_result).
     Returns string - calculation result formatted according to requirements"""

  def equation_tokens(values):
    """Returns generator of tokens to be used in equation formatting"""
    if values:
      # including minus sign with first element (if negative)
      yield str(values[0])
    for value in islice(values, 1, None): # using islice to not create intermediate list
      yield ('+' if value >= 0.0 else '-')
      yield str(abs(value))

  return 'result(%s) = %s\nerror-lines = %s' % (' '.join(equation_tokens(values)), total, errors_count)


def process_csv(filepath):
  """Given input file path returns csv processing result as string"""
  rows = read_csv(filepath)
  result = calculate_result(rows)
  return format_result(result)


def main():
  print 'Testing extract_row_value'
  test(extract_row_value(['1',   '23.0', 'qwe' ]), 23.0)
  test(extract_row_value(['2',   '23.0', 'qwe' ]), None)
  test(extract_row_value(['-1',  '23.0', '10.0']), None)
  test(extract_row_value(['0',   '23.0', '10.0']), 0.0 )
  test(extract_row_value(['qwe', '23.0', '10.0']), None)

  print
  print 'Testing calculate_result'
  rows = [
    ['1', '12.0', 'qwe'],
    ['2', '10.0', 'qwe'],
    ['2', '10.0', '-20.0']]
  test(calculate_result(iter(rows)), ([12.0, -20.0], -8.0, 1))
  test(calculate_result([]), ([], 0.0, 0))

  print
  print 'Testing format_result'
  test(format_result(([10.0, 20.0, -30.0], 0.0, 2)),
       'result(10.0 + 20.0 - 30.0) = 0.0\nerror-lines = 2')
  test(format_result(([-10.0, 20.0, -30.0], 0.0, 1)),
       'result(-10.0 + 20.0 - 30.0) = 0.0\nerror-lines = 1')
  test(format_result(([10.0], 10.0, 0)),
       'result(10.0) = 10.0\nerror-lines = 0')
  test(format_result(([], 0.0, 10)),
       'result() = 0.0\nerror-lines = 10')

  print
  print 'Testing complete processing'
  test(process_csv('in2_1.txt'), 'result(5.2 - 3.14 + 0.0) = 2.06\nerror-lines = 3')
  test(process_csv('in2_2.txt'), 'result(-3.1 - 1.0) = -4.1\nerror-lines = 0')
  test(process_csv('in2_3.txt'), 'result(0.75) = 0.75\nerror-lines = 0')
  test(process_csv('in2_4.txt'), 'result(0.0) = 0.0\nerror-lines = 0')
  test(process_csv('in2_5.txt'), 'result() = 0.0\nerror-lines = 1')


if __name__ == '__main__':
  main()
