from reportgeneratorbase import *

class ReportGeneratorCsv(ReportGeneratorBase):

  @property
  def dbpath(self):
    return self.__dbpath


  def __init__(self, dbpath):
    self.__dbpath = dbpath


  def print_data(self, *args):
    csv_path, mark_report, month_report = args
    try:
      output_file = open(csv_path, "w")
    except IOError:
      return 'Input file not found'
    if mark_report:
      map(lambda x: output_file.write(x), self.generate_marks_report(mark_report))
      output_file.write('\n')
    if month_report:
      map(lambda x: output_file.write(x), self.generate_month_report(month_report))
    output_file.close()


  def save_to_db(self, tests):
    conn = sqlite3.connect(self.__dbpath)
    c = conn.cursor()

    # clear tables before run
    c.execute('delete from logins')
    c.execute('delete from tests')
    c.execute('delete from results')

    logins = {}
    statuses = {}
    for test in tests:
      if test.login not in logins:
        c.execute('INSERT INTO logins values (NULL, "' + test.login + '")')
        logins[test.login] = c.lastrowid

      if test.status not in statuses:
        c.execute('INSERT INTO tests values (NULL, "' + test.status + '")')
        statuses[test.status] = c.lastrowid

      c.execute('INSERT INTO results values (?, ?, ?, ?)',
               (logins[test.login], statuses[test.status], test.date, test.mark))
    conn.commit()
    conn.close()


  def get_mean_marks(self):
    conn = sqlite3.connect(self.__dbpath)
    c = conn.cursor()
    script = '''
            select l.name, ROUND(AVG(CAST(r.mark as float)), 2) as mark
              from logins l
              join results r
              on r.loginId = l.id
              group by name
              order by mark desc'''
    mean_marks = []
    try:
      mean_marks = c.execute(script).fetchall()
    except sqlite3.OperationalError, msg:
      print 'get_mean_marks method--> sqlite3.OperationalError. Message: {0}'.format(msg)
    conn.close()
    return mean_marks


  def get_month_report(self):
    conn = sqlite3.connect(self.__dbpath, detect_types=sqlite3.PARSE_DECLTYPES)
    c = conn.cursor()
    # returned data in format: Status | Date | Count
    script = '''
            select t.name as status, r.date, count(t.name) as count
              from tests t
              join results r
              on t.id = r.testId
              group by t.name, r.date
              order by r.date'''
    report = []
    try:
      report = c.execute(script).fetchall()
    except sqlite3.OperationalError, msg:
      print 'get_month_report method--> sqlite3.OperationalError. Message: {0}'.format(msg)
    conn.close()
    return report


  def generate_marks_report(self, mean_marks):
    lines = []
    COL1_LEN = 15
    COL2_LEN = 10
    SEP_LINE = '-' * (COL1_LEN + COL2_LEN)
    marks = []
    columns = [ ('Login', COL1_LEN, '<'), ('Mark', COL2_LEN, '>') ]
    lines.append("%s\n" % 'Mean mark report')
    lines.append("%s\n" % SEP_LINE)
    table_header = '|'.join('{0:^{1}}'.format(column[0], column[1]) for column in columns)
    lines.append("%s\n" % table_header)
    lines.append("%s\n" % SEP_LINE)

    for login, mark in mean_marks:
      marks.append(float(mark))
      row = [(login, COL1_LEN, '<'),
             (mark, COL2_LEN, '>')]
      formatted_row = '|'.join('{0:{1}{2}}'.format(item[0], item[2], item[1]) for item in row)
      lines.append("%s\n" % formatted_row)

    lines.append("%s\n" % SEP_LINE)

    median = get_median(marks)
    lq = get_lower_quartile(marks)
    uq = get_upper_quartile(marks)

    for title, mark in [('median', median), ('lower', lq), ('upper', uq)]:
      row = [(title, COL1_LEN, '<'),
             (mark, COL2_LEN, '>')]
      formatted_row = '|'.join('{0:{1}{2}}'.format(item[0], item[2], item[1]) for item in row)
      lines.append("%s\n" % formatted_row)

    lines.append("%s\n" % SEP_LINE)
    return lines


  def generate_month_report(self, month_report):
    lines = []
    COL1_LEN = 15
    COL2_LEN = 15
    COL3_LEN = 10
    SEP_LINE = '-' * (COL1_LEN + COL2_LEN + COL3_LEN)
    columns = [ ('Status', COL1_LEN, '<'), ('Date', COL2_LEN, '>'), ('Count', COL3_LEN, '>') ]
    lines.append("%s\n" % 'Month report')
    lines.append("%s\n" % SEP_LINE)
    table_header = '|'.join('{0:^{1}}'.format(column[0], column[1]) for column in columns)
    lines.append("%s\n" % table_header)
    lines.append("%s\n" % SEP_LINE)

    for status, date, count in month_report:
      row = [(status, COL1_LEN, '<'),  (date.strftime("%Y/%m/%d"), COL2_LEN, '>'), (count, COL3_LEN, '>')]
      formatted_row = '|'.join('{0:{1}{2}}'.format(item[0], item[2], item[1]) for item in row)
      lines.append("%s\n" % formatted_row)

    lines.append("%s\n" % SEP_LINE)
    return lines


  def get_data(self, folder_path):
    tests = []
    for filepath in self.get_files(folder_path):
      tests.extend(self.get_testresults(filepath))


    return tests


  def get_testresults(self, file_path):
    tests = []
    with open(file_path, 'rU') as input_file:
      for rec in csv.reader(input_file, delimiter=';'):
        if not len(rec):
            continue
        try:
          test = TestResultInteger(*rec)
          tests.append(test)
        except TestResultException as ex:
          print ex.message
    return tests


  def check_file(self, dirpath, filename):
    return filename.startswith('in') and filename.endswith('.csv')


  def get_files(self, *args):
    for dirpath, dirnames, filenames in os.walk(*args):
      for filename in filenames:
        if self.check_file(dirpath, filename):
          yield os.path.join(dirpath, filename)
