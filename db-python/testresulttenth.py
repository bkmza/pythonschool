from testresultbase import TestResultBase
from testresultexception import TestResultException

class TestResultTenth(TestResultBase):

  def __init__(self, *args):

    login, status, date, mark = args
    TestResultBase.login = self.parse_login(login)
    TestResultBase.status = self.parse_status(status)
    TestResultBase.date = self.parse_date(date)
    TestResultBase.mark = self.parse_mark(mark)

  #task 3. 0.1 marks. range 0-10.
  def parse_mark(self, mark):
    try:
      mark = float(mark)
    except ValueError:
      raise TestResultException('Mark cannot parsed to float')

    if mark % 0.1:
      raise TestResultException('Mark should be integer or divided on 0.5')

    if mark > 10 or mark < 0:
      raise TestResultException('Mark cannot be less zero or greater 10')

    return mark