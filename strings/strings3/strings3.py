import re
from datetime import datetime

TIME_FORMAT_USA = '%B %d, %Y'
DATE_PARTS = 5
MAX_REPLACE_COUNT = 1
YEAR_LONG_LENGTH = 4
DAY_FORMAT = '%d'
MONTH_FORMAT = '%m'
YEAR_LONG_FORMAT = '%Y'
YEAR_SHORT_FORMAT = '%y'
EXCHANGE_TYPES = ['belarusian roubles', 'blr', 'dollar']

def process(file_in, file_out):
  try:
      input_file = open(file_in, "r")
      output_file = open(file_out, "w")
  except:
      return 'Input file not found'

  for line in input_file.readlines():
    pline = parse_line(line)
    output_file.write("%s\n" % pline)
  
  input_file.close()
  output_file.close()
  return 'Data added to out.txt'

def parse_line(sline):
  sline = replace_dates(sline)
  sline = replace_sums(sline)
  return sline

def replace_dates(sline):
  dates = re.findall(r'\b(\d{1,2}([.-/])\d{1,2}[.-/](\d{2,4}))\b', sline)
  for sdate, ssep, syear in dates:
    if len(sdate):
      try:
        tyear = YEAR_LONG_FORMAT if len(syear) == YEAR_LONG_LENGTH else YEAR_SHORT_FORMAT
        date = datetime.strptime(sdate, '%s' * DATE_PARTS % (DAY_FORMAT, ssep, MONTH_FORMAT, ssep, tyear))
        sline = sline.replace(sdate, date.strftime(TIME_FORMAT_USA), MAX_REPLACE_COUNT)
      except:
        pass
  return sline
      
def replace_sums(sline):
  sums = re.findall(r'(([1-9][0-9\s]+)(%s))' % '|'.join(EXCHANGE_TYPES), sline)
  for sres, ssum, sexchange in sums:
    if len(sres):
      try:
        sum = ''.join(ssum.split())
        sline = sline.replace(sres, '%s %s' % (sum, sexchange)) 
      except:
        pass
  return sline
               
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'strings3'
  test(process('in.txt', 'out.txt'), 'Data added to out.txt')

if __name__ == '__main__':
  main()

