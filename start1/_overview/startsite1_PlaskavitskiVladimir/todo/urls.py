from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='todo/today/')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    (r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^paranoia.admin/', include(admin.site.urls)),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/')),
    url(r'^users/', RedirectView.as_view(url='/')),
    url(r'^todo/', include('todolist.urls', namespace="todolist")),
)
