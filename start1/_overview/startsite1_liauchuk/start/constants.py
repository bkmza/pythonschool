class STATUS:
    TODAY = 'today'
    SOMEDAY = 'someday'
    FIXED = 'fixed'

    STATUSES = [TODAY, SOMEDAY, FIXED]
