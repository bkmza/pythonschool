import csv

MINUS = "-"
PLUS_WS = " + "
MINUS_WS = " - "
MINUS_LENGTH = len(MINUS)
START_INDEX = 0
FIRST_ELEMENT = 0

def process(file_name):
    try:
        input_file = open(file_name, "rb")
    except:
        return 'Input file not found'

    sum = 0
    errors = 0
    correct_values = []
    tokens = []
    
    # Stage 1. Generating correct values list
    reader = csv.reader(input_file, delimiter=';')
    for row in reader:
        try:
            index = int(row[START_INDEX])
            correct_values.append(float(row[index]))        
        except:
            errors += 1
    input_file.close()

    # Stage 2. Calculating sum of values and signs
    for fval in correct_values:
        sum += fval
        tokens.append(PLUS_WS if fval >= 0 else MINUS_WS)
        tokens.append(str(abs(fval)))
        
    if len(tokens):
        MINUS_WS_LENGTH = len(MINUS_WS)
        SIGN_POS = MINUS_WS.index(MINUS)
        symbol = tokens[FIRST_ELEMENT][SIGN_POS]
        tokens[FIRST_ELEMENT] = tokens[FIRST_ELEMENT][MINUS_WS_LENGTH:]
        if symbol == MINUS:
            tokens[FIRST_ELEMENT] = MINUS + tokens[FIRST_ELEMENT]
        
    # Stage 3. Generating output string result
    return '''result(%s) = %s error-lines = %d''' % (
        ''.join(tokens), '0.0' if sum == 0 else '{0:g}'.format(sum), errors)

                                             
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'strings1'
  test(process('test0.csv'), 'Input file not found')
  test(process('test1.csv'), '''result(5.2 - 3.14 + 0.0) = 2.06 error-lines = 3''')
  test(process('test2.csv'), '''result(-3.1 - 1.0) = -4.1 error-lines = 0''')
  test(process('test3.csv'), '''result(0.75) = 0.75 error-lines = 0''')
  test(process('test4.csv'), '''result(0.0) = 0.0 error-lines = 0''')
  test(process('test5.csv'), '''result() = 0.0 error-lines = 1''')

if __name__ == '__main__':
  main()

