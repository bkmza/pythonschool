class CsvLineException(Exception):
    
    def __init__(self, message):
        self._message = message

    def __str__(self):
        return self._message

class EmptyCsvLineException(CsvLineException):
        
    def __init__(self, field):
        CsvLineException.__init__(self, 'Empty argument for ' + field)


class ValueCsvLineException(CsvLineException):
        
    def __init__(self, field):
        CsvLineException.__init__(self, 'Wrong argument for ' + field)


class NonpositiveCsvLineException(CsvLineException):
        
    def __init__(self, field):
        CsvLineException.__init__(self, 'Nonpositive value for ' + field)


