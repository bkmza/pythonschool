def is_simply_digit(y):
  x = y // 2
  while x > 1:
    if y % x == 0:
      print '%s has factor %s' % (y, x)
      break
    x -= 1
  else:
    print '%s is prime' % y

is_simply_digit(13)
is_simply_digit(13.0)
is_simply_digit(15)
is_simply_digit(15.0)