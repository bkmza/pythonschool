from Life import Life
from Parom import Parom
from Company import Company

def main():
    parom = Parom()
    objs = Life.get_case(parom.seats)
    packs = Company.packing(objs)
    seq = Company.sorting(packs)
    check = Company.checking_weight(seq, parom)
    print "start > ", objs
    print "packs > ", packs
    print "sort > ", seq
    print "possible >", check


if __name__ == '__main__':
    main()