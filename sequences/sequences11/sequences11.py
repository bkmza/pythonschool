import re
import math

REGEX_FLOAT = r'([eE\d.-]+)'
REGEX_POINT = r'\(\s*%s\s*\;\s*%s\s*\)' % (REGEX_FLOAT, REGEX_FLOAT)
REGEX_LINE = r'\s*%s\s*%s\s*' % (REGEX_POINT, REGEX_POINT)

def process(file_in):
  try:
      input_file = open(file_in, "r")
  except:
      return 'Input file not found'
    
  lens = []
  for line in input_file:
    for x1, y1, x2, y2 in re.findall(REGEX_LINE, line):
      len = get_line_length(x1,y1,x2,y2)
      rlen = int(round(len))
      if rlen not in lens:
        lens.append(rlen)
  
  input_file.close()
  return sorted(lens)

def get_line_length(x1,y1,x2,y2):
  return math.sqrt((float(x1) - float(x2)) ** 2 + ((float(y1) - float(y2)) ** 2))
      
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
  print 'sequences1'
  test(process('not_exists_file.txt'), 'Input file not found')
  test(process('in.txt'), [1, 3])

if __name__ == '__main__':
  main()

