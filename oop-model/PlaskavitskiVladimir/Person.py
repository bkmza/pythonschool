from decorators import repr


@repr
class Person():

    @property
    def weight(self):
        return self.__weight

    def __init__(self, weight=42):
        self.__weight = weight

    def __repr__(self):
        return "{0} ({1:0.2f})".format(self.__class__.__name__, self.weight)