__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'

from Cargo import Cargo


class SeaContainer(Cargo):

    def __init__(
            self,
            name="Sea Container",
            density=300,
            inner_height=2.350,
            inner_length=5.887,
            inner_width=2.330,
            self_weight=1000
    ):
        self._density = density
        self._inner_height = inner_height
        self._inner_length = inner_length
        self._inner_width = inner_width
        self._self_weight = self_weight
        super(SeaContainer, self).__init__(name)

    def _calculate_weight(self):
        return self._self_weight + self._density * self._inner_height * self._inner_length * self._inner_width

