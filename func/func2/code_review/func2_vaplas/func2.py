import csv
    
SING_PLUS = "+"
SING_MINUS = "-"
BEFORE_SING = " "
AFTER_SING = " "
PLUS = '%s%s%s' % (BEFORE_SING, SING_PLUS, AFTER_SING)
MINUS = '%s%s%s' % (BEFORE_SING, SING_MINUS, AFTER_SING)

def scan(path):
    try: 
        with open(path) as f:
            reader = csv.reader(f, delimiter=';')        
            for row in reader:            
                try:
                    value = float(row[int(row[0])])
                except (ValueError, IndexError):              
                    value = None                    
                yield value
    except:
        print 'problem with "%s"' % path

def get_number(path):
    
    global errors, amount
    
    errors = 0
    amount = .0
    
    for value in scan(path):
        if value is None:
            errors += 1
        else: 
            amount += value
            yield value    

def formatter(x):
    return '%s%s' % ((MINUS, abs(x)) if x < 0 else (PLUS, x))

def proccess(path):
    expression = "".join(map(formatter, get_number(path)))    
    if expression:    
        expression = expression[len(PLUS):] if expression.startswith(PLUS) else SING_MINUS + expression[len(MINUS):]       
    return "result({0}) = {1}".format(expression, amount) + "\n" + "error-lines = %d" % errors
            
def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))

def main():
    
    test(proccess("tests/input1.csv"),"result(5.2 - 3.14 + 0.0) = 2.06\nerror-lines = 3")
    test(proccess("tests/input2.csv"),"result(-3.1 - 1.0) = -4.1\nerror-lines = 0")
    test(proccess("tests/input3.csv"),"result(0.75) = 0.75\nerror-lines = 0")
    test(proccess("tests/input4.csv"),"result(0.0) = 0.0\nerror-lines = 0")
    test(proccess("tests/input5.csv"),"result() = 0.0\nerror-lines = 1")
        
if __name__ == '__main__':
    main()