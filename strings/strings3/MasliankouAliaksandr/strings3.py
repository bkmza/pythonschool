import sys
import re
import datetime
import locale

input = r'in.txt'
output = r'out.txt'

CENTURY = '20' # is used for 2-digits format of year - to simplify will suppose it means current millennium :) 

date_pattern = re.compile(r'(\d{1,2})([./-])(\d{1,2})\2(\d{2,4})')
money_pattern = re.compile(r'(\b\d{1,3} *(\d{3} *)*)(?=blr\b|belarusian roubles\b)')
    
def format_text(): 
    try:
        fin = open(input, 'r')
        fout = open(output, 'w')
        
        for line in fin:
            fout.write(reformat(money_pattern, reformat_money, 
                                reformat(date_pattern, reformat_date, line)))
            fout.flush()
    except IOError:
        sys.exit('Terminated: file %s not found' % input);
    else: 
        fin.close()
        fout.close()

def reformat(pattern, func, line):
    return pattern.sub(func, line) 

# return formatted line with money
# add ' ' to the end of formatted value 
def reformat_money(match):
    return match.group().replace(' ', '') + ' '

# return formatted date    
def reformat_date(match):
    day = match.group(1)
#     delimiter = match.group(2)
    month = match.group(3)
    year = match.group(4)
    if len(year)==2: 
        year = CENTURY + year
    return datetime.datetime(int(year), int(month), int(day)).strftime('%B %d, %Y')

def main():
    locale.setlocale(locale.LC_ALL, ('en_US', 'UTF-8'))
    format_text()

if __name__ == "__main__":
    main()
