from django.conf.urls import patterns, url

from start import views

urlpatterns = patterns('',
    url(r'^$', views.todo_list, name='tasks'),
    url(r'^(?P<status>.+)/add$', views.add_task, name='add'),
    url(r'^(?P<status>.+)/handle$', views.handle_tasks, name='handle'),
    url(r'^(?P<status>.+)/$', views.todo_list, name='tasks'),
)

