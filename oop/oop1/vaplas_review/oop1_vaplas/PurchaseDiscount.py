from Purchase import Purchase

class PurchaseDiscount(Purchase):
    
    @property    
    def discount(self):
        return self.__discount
  
    def __init__(self, name, price, qty, discount):
        Purchase.__init__(self, name, price, qty)
        self.__discount = discount

    def calculating_discount(self):        
        raise NotImplementedError()
        
    def get_cost(self):        
        return Purchase.get_cost(self) - self.calculating_discount()
    
    def __str__(self):
        return Purchase.__str__(self) + ";{0}".format(self.discount)
    