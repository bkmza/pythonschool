from decorators import repr


@repr
class Solid():

    @property
    def weight(self):
        return self.__weight

    @property
    def out_metrics(self):
        return self.__out_metrics


    def __init__(self, width=1, height=1, depth=1, density=2000):
        self.__out_metrics = (width, height, depth)
        self.__weight = width * height * depth * density