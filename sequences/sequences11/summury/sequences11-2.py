import sys
import math
import re
import bisect


def get_segment_len(line, re_coords=re.compile(r'[\s();]+')):
    coords = re_coords.split(line)[1:-1]
    diff_x = float(coords[2]) - float(coords[0])
    diff_y = float(coords[3]) - float(coords[1])
    return  int(math.hypot(diff_x, diff_y) + 0.5)


def get_sequence(filename):
    sequence = [-1]
    with open(filename, 'rU') as f:
        for line in f:
            append_to_sorted_list(sequence, get_segment_len(line))
    del sequence[0]
    return sequence

  
def append_to_sorted_list(sequence, segment_len):
    index = bisect.bisect(sequence, segment_len)
    if sequence[index-1] != segment_len:
        sequence.insert(index, segment_len)                                                          


def main(filename):
    try:
        sequence = get_sequence(filename)
        print sequence
    except IOError:
        print 'wrong filename'


if __name__ == '__main__':
    main(sys.argv[1])
