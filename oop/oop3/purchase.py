from purchaseexception import PurchaseException
from utils import test
from commodity import Commodity

class Purchase(object):
  
  @property
  def commodity(self):
    return self.__commodity
  
  @property
  def quantity(self):
    return self.__quantity
  
  def __init__(self, *args):
    if len(args) > 3:
      raise PurchaseException(0, 'Too many agruments')
    if len(args) <= 2:
      raise PurchaseException(0, 'Too few agruments')

    name, price, quantity = args
    self.__commodity = Commodity(name, price)

    try:
      int_quantity = int(quantity)
    except ValueError:
      raise PurchaseException(0, 'Cannot convert quantity to int base 10')

    if int_quantity <= 0:
      raise PurchaseException(0, 'Quantity cannot be less or equal zero')

    self.__quantity = int_quantity


  def get_cost(self):
    return self.commodity.price * self.quantity


  def __str__(self):
    props = [self.commodity.name, self.commodity.price, self.quantity]
    return ';'.join(map(lambda x: str(x), props))
  
  def __eq__(self, that):
    return self.commodity == that.commodity
  
  def __repr__(self):
    return "%s;%s" % (self.__class__.__name__, self.__str__()) 
   

if __name__ == '__main__':
  # Purchase
  p1 = Purchase('Snikers', price=5, quantity=10)
  p2 = Purchase('Mars', price=4, quantity=5)
  print p1.__class__.__name__
  test(p1.__str__(), 'Snikers;5;10')
  test(p2.__str__(), 'Mars;4;5')
