from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from django.forms.widgets import DateInput

from start.forms import TaskForm
from start.models import Task
from start.constants import STATUS

from datetime import datetime, timedelta


# Create your views here.
def todo_list(request, status=STATUS.TODAY):
    if status not in STATUS.STATUSES:
        status = STATUS.TODAY
    if status == STATUS.FIXED:
        queryset = Task.objects.filter(is_fixed=True)
    else:
        tomorrow = datetime.today() + timedelta(days=1)
        queryset = Task.objects.filter(is_fixed=False)
        if status == STATUS.SOMEDAY:
            queryset = queryset.filter(fix_date__gte=tomorrow)
        else:
            queryset = queryset.filter(fix_date__lt=tomorrow)
    queryset = queryset.order_by('fix_date')
    context = {'tasks': queryset, 'status': status, 'user': request.user.username}
    return render(request, 'start/tasks.html', context)


def add_task(request, status=STATUS.SOMEDAY):
    if status not in STATUS.STATUSES[:2]:
        status = STATUS.SOMEDAY 
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
#             cd = form.cleaned_data    # when inherited from AddTaskForm
#             task = Task.objects.create()
#             task.content = cd['content']
#             task.fix_date = cd['fix_date']
#             task.is_fixed = False
#             task.save()
            form.save()
            return HttpResponseRedirect(reverse('start:tasks', args=[status]))
    else:
        form = TaskForm()
    init_date = datetime.today()
    attrs = {}
    if status == STATUS.TODAY:
        attrs['readonly'] = 'readonly'
    else:
        init_date += timedelta(days=1)
    form.fields['fix_date'].initial = init_date
    form.fields['fix_date'].widget.attrs = attrs
    context = RequestContext(request, {'form': form, 'status': status})
    return render_to_response('start/add_task.html', context)


def handle_tasks(request, status=STATUS.TODAY):
    if status not in STATUS.STATUSES:
        status = STATUS.TODAY
    id_tasks = request.POST.getlist('task_id')
    id_num_tasks = filter(lambda x: x.isdigit(), id_tasks)
    command = {
            'fix': lambda x: x.update(is_fixed=True), 
            'remove': lambda x: x.delete()
        }.get(request.POST['action'], None)
    if (not id_num_tasks) or (not command):
        return todo_list(request, status)
    command(Task.objects.filter(pk__in=id_num_tasks))
    return HttpResponseRedirect(reverse('start:tasks', args=[status]))
    
