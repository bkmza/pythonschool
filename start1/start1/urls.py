from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.contrib import admin
admin.autodiscover()
from polls import views


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/polls/list/today')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/polls/accounts/login'}),
    (r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^paranoia.admin/', include(admin.site.urls)),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/polls/list/today')),
    url(r'^users/', RedirectView.as_view(url='/polls/list/today')),

    url(r'^polls/', include('polls.urls', namespace="polls")),
    url(r'^paranoia.admin/', include(admin.site.urls)),
)