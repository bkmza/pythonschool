#!/usr/bin/python
from __future__ import print_function
from itertools import islice 

import csv
import sys


def get_lines(filename, delimiter=';'):
    '''
    Parses csv file with the given `filename` and returns its lines as lists.
    '''
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter)
        for row in reader:
            yield row


def get_value(line):
    '''
    Parses the `line` and returns float value, pointed by the first item.
    Returns None if the `line` cannot be parsed.
    '''
    try:
        value_position = int(line[0])
        return float(line[value_position])
    except (IndexError, ValueError) as e:
        return None


def get_summary(values):
    '''
    Returns summary (correct values, total sum, number of invalid rows)
    for the given `values`.
    Summary is a dict with keys "values", "total" and "errors_count"
    '''
    correct_values = [v for v in values if not v is None]
    total = float(sum(correct_values))
    errors_count = len(values) - len(correct_values)
    return {
        'values': correct_values,
        'total': total,
        'errors': errors_count,
    }


def get_report(summary):
    '''
    Returns report as a string.
    '''
    def get_tokens(values):
        if values:
            yield str(values[0])
            for value in islice(values, 1, len(values)):
                yield '-' if value < 0 else '+'
                yield str(abs(value))
    tokens = get_tokens(summary['values'])
    expression = ' '.join(tokens)

    return (
        'result(%(expression)s) = %(total)s\n'
        'error-lines = %(errors)s'
    ) % {
        'expression': expression,
        'total': summary['total'],
        'errors': summary['errors'],
    }


def main(filename):
    lines = get_lines(filename)
    values = [get_value(line) for line in lines]
    summary = get_summary(values)
    print(get_report(summary))


if __name__ == '__main__':
    main(sys.argv[1])
