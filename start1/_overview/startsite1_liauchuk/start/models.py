from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Task(models.Model):
    content = models.CharField(max_length=50)
    fix_date = models.DateField()
    is_fixed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.content
    
