from abc import ABCMeta, abstractmethod
from math import pi

class Transportable(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_mass(self):
        pass


class Person(Transportable):

    def __init__(self, name, mass):
        self._name = name
        self._mass = mass
        
    def get_mass(self):
        return self._mass
    
    def __str__(self):
        return 'person;%s;%.1f' % (self._name, self._mass)


class AbstractCargo(object):
    __metaclass__ = ABCMeta
    
    def __init__(self, name):
        self._name = name

    def __str__(self):
        return self._name


class AbstractMaterial(AbstractCargo):
    __metaclass__ = ABCMeta
    
    def __init__(self, name, density):
        AbstractCargo.__init__(self, name)
        self._density = density

    def get_density(self):
        return self._density

    def __str__(self):
        return '%s;%.1f' % (super(AbstractMaterial, self), self._density)


class AbstractTare(Transportable):
    __metaclass__ = ABCMeta
    
    def __init__(self, material, own_mass):
        self._material = material
        self._own_mass = own_mass

    @abstractmethod
    def get_volume(self):
        pass
    
    def get_mass(self):
        return self._own_mass + self._material.get_density() * self.get_volume()
    
    def __str__(self):
        return '%s;%.2f;%.2f' % (str(self._material), self._own_mass, self.get_mass())
    
    
class Cargo(AbstractCargo, Transportable):

    def __init__(self, name, mass):
        AbstractCargo.__init__(self, name)
        self._mass = mass

    def get_mass(self):
        return self._mass
    
    def __str__(self):
        return 'cargo;%s;%.2f' % (super(Cargo, self).__str__(), self.get_mass())
    
    
class Liquid(AbstractMaterial):

    def __init__(self, name, density):
        AbstractMaterial.__init__(self, name, density)

    def __str__(self):
        return 'liquid;%s' % (super(AbstractMaterial, self).__str__())
    
    
class Solid(AbstractMaterial):

    def __init__(self, name, density):
        AbstractMaterial.__init__(self, name, density)

    def __str__(self):
        return 'solid;%s' % (super(AbstractMaterial, self).__str__())
    
    
class Cistern(AbstractTare):
    _LENGTH = 12.2
    _R = 1.8
    _OWN_MASS = 0.6

    def __init__(self, liquid):
        if not isinstance(liquid, Liquid):
            raise RuntimeError(str(liquid) + " is not a liquid")
        AbstractTare.__init__(self, liquid, Cistern._OWN_MASS)
        
    def get_volume(self):
        return round(pi * Cistern._R * Cistern._R * Cistern._LENGTH * 100.0) / 100.0

    
class Container(AbstractTare):
    _HEIGHT = 2.7
    _LENGTH = 8.5
    _WIDTH = 3.4
    _OWN_MASS = 0.4

    def __init__(self, solid):
        if not isinstance(solid, Solid):
            raise RuntimeError(str(solid) + " is not a solid")
        AbstractTare.__init__(self, solid, Container._OWN_MASS)
        
    def get_volume(self):
        return round(Container._LENGTH * Container._WIDTH * Container._HEIGHT * 10.0) / 10.0

    
class Ferry(object):
    _MAX_MASS = 50000
    
    def __init__(self, items):
        for item in items:
            if not isinstance(item, Transportable):
                raise RuntimeError("Bad item for ferry >> " + str(item))
        self._items = items
        
    def is_overloaded(self):
        return sum(item.get_mass() for item in self._items) > Ferry._MAX_MASS
    
    def show(self):
        print '     Entities on ferry'
        print '\n'.join(str(item) for item in self._items)
        print '-' * 30
            
    __TRANSPORTABLES = [cls.__name__ for cls in [Person, Container, Cargo, Cistern]]

    def sort(self):
        self._items = sorted(self._items, key = lambda(item): Ferry.__TRANSPORTABLES.index(item.__class__.__name__))


def main():
    auto = Cargo("Auto", 1500)
    blackBox0 = Cargo("Black-box", 780)
    blackBox1 = Cargo("Black-box", 300)
    person0 = Person("Bob", 0.075)
    person1 = Person("Tim", 0.09)
    person2 = Person("Jack", 0.08)
    coal0 = Solid("Coal", 2.5)
    coal1 = Solid("Coal", 2.5)
    cont0 = Container(coal0)
    cont1 = Container(coal1)
    oil0 = Liquid("Oil", 0.95)
    oil1 = Liquid ("Oil", 0.97)
    cist0 = Cistern(oil0)
    cist1 = Cistern(oil1)
    
    items = [
        person1, blackBox0, auto, person0, cont0, cist0, cont1, cist0, cist0, 
        cist1, cont0, person2, person1, blackBox1
    ]
    ferry = Ferry(items)
    ferry.show()
    print 'is overloaded: %s' % ferry.is_overloaded()
    ferry.sort()
    print 'After sorting'
    ferry.show()

if __name__ == '__main__':
    try:
        main()
    except RuntimeError as e:
        print e