from utils import test
from commodity import Commodity

class Purchase(object):
  
  @property
  def commodity(self):
    return self.__commodity
  
  @property
  def quantity(self):
    return self.__quantity
  
  def __init__(self, name, price, quantity):
    self.__commodity = Commodity(name, price)
    self.__quantity = int(quantity)
  
  def get_cost(self):
    raise NotImplementedError("Please Implement this method")

  def __str__(self):
    props = [self.commodity.name, self.commodity.price, self.quantity]
    return ';'.join(map(lambda x: str(x), props))
  
  def __eq__(self, that):
    return self.commodity == that.commodity
  
  def __repr__(self):
    return "%s;%s" % (self.__class__.__name__, self.__str__()) 
   

if __name__ == '__main__':
  # Purchase
  p1 = Purchase('Snikers', price=5, quantity=10)
  p2 = Purchase('Mars', price=4, quantity=5)
  print p1.__class__.__name__
  test(p1.__str__(), 'Snikers;5;10')
  test(p2.__str__(), 'Mars;4;5')
