from utils import test
from purchase import Purchase
from purchaseexception import BulkPurchaseException

class BulkPurchase(Purchase):
  
  __DISCOUNT_CONDITION = 15
  
  @property
  def discount(self):
    return self.__discount
  
  def __init__(self, *args):
    if len(args) > 4:
      raise BulkPurchaseException(0, 'Too many agruments')
    if len(args) <= 3:
      raise BulkPurchaseException(0, 'Too few agruments')

    name, price, quantity, discount = args

    if not discount:
      raise BulkPurchaseException(16, 'Discount should exist')

    try:
      float_discount = float(discount)
    except ValueError:
      raise BulkPurchaseException(0, 'Cannot convert discount to float')

    Purchase.__init__(self, name, price, quantity)
    self.__discount = float_discount

  def get_cost(self):
    cost = self.commodity.price * self.quantity
    if self.quantity > BulkPurchase.__DISCOUNT_CONDITION:
      cost *= (1 - self.discount/100)
    return int(cost + 0.5)

  def __str__(self):
    return Purchase.__str__(self) + ';%s;%s' % (self.discount, self.get_cost())


if __name__ == '__main__':
  print
  bp1 = BulkPurchase('Christmas tree', price=500, count=20, discount=5.825)
  bp2 = BulkPurchase('Deadtree book', 500, 10, 17.5)
  print bp1.__class__.__name__
  test(bp1.__str__(), 'Christmas tree;500;20;5.825;9418')
  test(bp2.__str__(), 'Deadtree book;500;10;17.5;5000')
  test(bp1.get_cost(), 9418)
  test(bp2.get_cost(), 5000)
  test(bp1.__eq__(bp2), False)
  test(bp2.__eq__(bp1), False)