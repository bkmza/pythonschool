import csv

MINUS = "-"
PLUS_WS = " + "
MINUS_WS = " - "
PLUS_WS_LENGTH = len(PLUS_WS)
START_INDEX = 0
MAX_REPLACE_COUNT = 1
settings = {}

def run_process(file_name):
  global settings 
  settings = { 'sum': 0 }
  try:
    lines = get_lines(file_name)
    parsed_values = (get_value(line) for line in lines)
    tokens = list(processing_value(fval) for fval in parsed_values)
  except IOError as e:
    return "I/O error({%s}): {%s}" % (e.errno, e.strerror)
  return get_statistic(tokens)

def get_statistic(items):
  tokens = [i for i in items if not (i is None)]
  errors_count = len(items) - len(tokens)
  if len(tokens):
    tokens[0] = tokens[0][PLUS_WS_LENGTH:] if tokens[0].startswith(PLUS_WS) else tokens[0].replace(MINUS_WS, MINUS, MAX_REPLACE_COUNT)
  return '''result(%s) = %s error-lines = %d''' % (
         ''.join(tokens), '0.0' if settings['sum'] == 0 else '{0:g}'.format(settings['sum']), errors_count)

def processing_value(fval):
  if not (fval is None):
    settings['sum'] += fval
    return '%s%s' % (PLUS_WS if fval >= 0 else MINUS_WS, str(abs(fval)))
  return None

def get_value(line):
  try:
    index = int(line[START_INDEX])
    return float(line[index])
  except (IndexError, ValueError):
    return None

def get_lines(file_name):
  with open(file_name, 'r') as file_object:
    for line in csv.reader(file_object, delimiter=';'):
      yield line
      
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))

def main():
  print 'func2'
  test(run_process('test0.csv'), 'I/O error({2}): {No such file or directory}')
  test(run_process('test1.csv'), '''result(5.2 - 3.14 + 0.0) = 2.06 error-lines = 3''')
  test(run_process('test2.csv'), '''result(-3.1 - 1.0) = -4.1 error-lines = 0''')
  test(run_process('test3.csv'), '''result(0.75) = 0.75 error-lines = 0''')
  test(run_process('test4.csv'), '''result(0.0) = 0.0 error-lines = 0''')
  test(run_process('test5.csv'), '''result() = 0.0 error-lines = 1''')

if __name__ == '__main__':
  main()

