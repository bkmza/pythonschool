class CommodityException(Exception):

  @property
  def line(self):
    return self._line

  @property
  def message(self):
    return self._message

  def __init__(self, line, message):
    self._line = line
    self._message = message

  def __str__(self):
     return self._message


class PurchaseException(CommodityException): pass
class PricePurchaseException(PurchaseException): pass
class BulkPurchaseException(PurchaseException): pass


