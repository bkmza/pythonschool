__author__ = 'Andrey Semikov <andrew.semikov@gmail.com>'


class Passenger(object):

    def __init__(self, name="Person", seat_number=1):
        self._name = name
        self.set_seat_number(seat_number)
        self._seat_number = seat_number

    @property
    def name(self):
        return self._name

    @property
    def seat_number(self):
        return self._seat_number

    def set_seat_number(self, seat_number):
        self._seat_number = seat_number

    def __str__(self):
        return "Passenger: %s" % self.name

    def __repr__(self):
        return self.__str__()