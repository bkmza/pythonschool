from django.db import models
from django.contrib.auth.models import User

class Task(models.Model):
  title = models.CharField(max_length=200)
  is_fixed = models.BooleanField(default=False)
  date = models.DateTimeField('date to fix')
  user = models.ForeignKey(User)

  def __unicode__(self):
    return self.title